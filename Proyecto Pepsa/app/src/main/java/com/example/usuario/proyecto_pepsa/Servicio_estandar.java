package com.example.usuario.proyecto_pepsa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Servicio_estandar extends AppCompatActivity {

    public static final String CURRENT_USER_INDEX_EXTRA = "COM.EXAMPLE.MAINACTIVITY.CURRENT_ASSEMBLIE";

    private int CurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_estandar);

        Intent intent = getIntent();
        CurrentUser = intent.getIntExtra(CURRENT_USER_INDEX_EXTRA,0);

    }
}
