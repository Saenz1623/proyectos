package com.example.usuario.proyecto_pepsa.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

@Database(entities = {Usuarios.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE = null;

    // SINGLETON
    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context,
                    AppDatabase.class, "usuarios.db")
                    .allowMainThreadQueries()
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);

                            db.execSQL("	INSERT INTO usuarios (id, username, password, name, address, cel) VALUES (0, 'Admin', 'Admin', 'Emanuel Saenz', 'Dirección', '1234567890');	");
                            db.execSQL("	INSERT INTO usuarios (id, username, password, name, address, cel) VALUES (1, 'Alex34', '7564', 'Alejandro Gomez', 'Dirección', '1234567890');	");
                            db.execSQL("	INSERT INTO usuarios (id, username, password, name, address, cel) VALUES (2, 'H45', '58794', 'Hernan Chan', 'Dirección', '1234567890');	");
                            db.execSQL("	INSERT INTO usuarios (id, username, password, name, address, cel) VALUES (3, 'LoL23', 'Yolo', 'Alfredo Huerta', 'Dirección', '1234567890');	");
                            db.execSQL("	INSERT INTO usuarios (id, username, password, name, address, cel) VALUES (4, 'Sal45', 'Azucar54', 'Ricardo Sansorez', 'Dirección', '1234567890');	");

                        }
                    })
                    .build();
        }

        return INSTANCE;
    }

    public abstract UsuariosDao usuariosDao();

}
