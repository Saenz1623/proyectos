package com.example.usuario.proyecto_pepsa.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UsuariosDao {

    @Insert
    public void InsertarUsuario(Usuarios usuarios);

    @Query("SELECT * FROM usuarios where username LIKE :username")
    public Usuarios GetUserByUsername(String username);

    @Query("SELECT * FROM usuarios")
    public List<Usuarios> GetAll();
}
