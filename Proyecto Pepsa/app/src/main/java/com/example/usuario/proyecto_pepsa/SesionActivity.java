package com.example.usuario.proyecto_pepsa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SesionActivity extends AppCompatActivity {

    public static final String CURRENT_USER_INDEX_EXTRA = "COM.EXAMPLE.MAINACTIVITY.CURRENT_ASSEMBLIE";

    private int CurrentUser;
    
    Button PrBtn;
    Button EsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesion);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PrBtn = findViewById(R.id.PremiumButton);
        EsBtn = findViewById(R.id.EstandarButton);

        Intent intent = getIntent();
        CurrentUser = intent.getIntExtra(CURRENT_USER_INDEX_EXTRA,0);

        EsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SesionActivity.this, Servicio_estandar.class);
                intent.putExtra(Servicio_estandar.CURRENT_USER_INDEX_EXTRA, CurrentUser);
                startActivity(intent);
            }
        });


    }
}
