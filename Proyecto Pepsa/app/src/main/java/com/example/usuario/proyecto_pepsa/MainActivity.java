package com.example.usuario.proyecto_pepsa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.usuario.proyecto_pepsa.db.AppDatabase;
import com.example.usuario.proyecto_pepsa.db.Usuarios;
import com.example.usuario.proyecto_pepsa.db.UsuariosDao;
import com.facebook.stetho.Stetho;

public class MainActivity extends AppCompatActivity {

    Button SignUpBtn;
    Button LoginBtn;

    EditText Username;
    EditText Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SignUpBtn = findViewById(R.id.sign);
        LoginBtn = findViewById(R.id.login);

        Username = findViewById(R.id.Usuario);
        Password = findViewById(R.id.Contraseña);


        Stetho.initializeWithDefaults(this);

        SignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Usuario = Username.getText().toString();
                String Passwo = Password.getText().toString();

                AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
                UsuariosDao UsDao = db.usuariosDao();

                Usuarios usuario = UsDao.GetUserByUsername(Usuario);

                if (usuario == null){
                    Toast t=Toast.makeText(getApplicationContext(),"Usuario no encontrado", Toast.LENGTH_SHORT);
                    t.show();
                }else {
                    String key = usuario.getPassword();
                    if (key.equals(Passwo)){
                        Toast t=Toast.makeText(getApplicationContext(),"Bienvenido " + usuario.getName(), Toast.LENGTH_SHORT);
                        t.show();

                    } else {
                        Toast t=Toast.makeText(getApplicationContext(),"Contraseña incorrecta", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }

            }
        });

    }
}
