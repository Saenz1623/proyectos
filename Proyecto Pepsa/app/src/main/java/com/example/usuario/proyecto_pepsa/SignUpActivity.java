package com.example.usuario.proyecto_pepsa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.proyecto_pepsa.db.AppDatabase;
import com.example.usuario.proyecto_pepsa.db.Usuarios;
import com.example.usuario.proyecto_pepsa.db.UsuariosDao;

import java.util.List;

public class SignUpActivity extends AppCompatActivity {

    EditText Username;
    EditText Password;
    EditText Name;
    EditText Addres;
    EditText Cel;
    TextView Matricula;
    Button Save;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Username = findViewById(R.id.Username);
        Password = findViewById(R.id.password);
        Name = findViewById(R.id.Name);
        Addres = findViewById(R.id.Address);
        Cel = findViewById(R.id.Cellphone);
        Matricula = findViewById(R.id.MatriculaText);

        Save = findViewById(R.id.Save);



        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        final UsuariosDao UsDao = db.usuariosDao();

        final List<Usuarios> usuarios = UsDao.GetAll();

        String Mat = String.valueOf(usuarios.size());

        Matricula.setText(Mat);

        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(SignUpActivity.this);
                dialogo1.setTitle("Importante");
                dialogo1.setMessage("¿Desea guardar este usuario?");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Usuarios usuarios1 = new Usuarios(0,"","","","","");
                        usuarios1.setAddres(Addres.getText().toString());
                        usuarios1.setCel(Cel.getText().toString());
                        usuarios1.setId(usuarios.size());
                        usuarios1.setName(Name.getText().toString());
                        usuarios1.setPassword(Password.getText().toString());
                        usuarios1.setUsername(Username.getText().toString());
                        UsDao.InsertarUsuario(usuarios1);
                        Toast t=Toast.makeText(getApplicationContext(),"Usuario guardado con éxito", Toast.LENGTH_SHORT);
                        t.show();
                        finish();
                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Toast t=Toast.makeText(getApplicationContext(),"Se ha cancelado la operación", Toast.LENGTH_SHORT);
                        t.show();
                    }
                });
                dialogo1.show();

            }
        });



    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("Si sale de esta ventana sus datos no seran guardados");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                SignUpActivity.super.onBackPressed();

            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                Toast t=Toast.makeText(getApplicationContext(),"Se ha cancelado la operación", Toast.LENGTH_SHORT);
                t.show();
            }
        });
        dialogo1.show();
    }

}
