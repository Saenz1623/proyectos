package com.example.usuario.proyecto_2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.AssembliesDao;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.Customers;
import com.example.usuario.proyecto_2.db.CustomersDao;
import com.example.usuario.proyecto_2.db.OrdenesRecy;
import com.example.usuario.proyecto_2.db.OrdenesRecyDao;
import com.example.usuario.proyecto_2.db.Order_assembliesDao;
import com.example.usuario.proyecto_2.db.Order_status;
import com.example.usuario.proyecto_2.db.Order_statusDao;
import com.example.usuario.proyecto_2.db.Orders;
import com.example.usuario.proyecto_2.db.OrdersDao;
import com.example.usuario.proyecto_2.db.Product_CategoriesDao;
import com.example.usuario.proyecto_2.db.Product_categories;
import com.example.usuario.proyecto_2.db.Products;
import com.example.usuario.proyecto_2.db.ProductsDao;
import com.example.usuario.proyecto_2.db.RecyclerPosition;
import com.example.usuario.proyecto_2.db.RecyclerPositionDao;

import java.util.ArrayList;
import java.util.List;

class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private Context context;


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView ProAssemText;
        private TextView ProdQtyText;
        private TextView ProPrzText;
        private int adapterposition;

        private ProductForAssem productForAssem;

        private int id;


        public ViewHolder (@NonNull final View itemView) {
            super(itemView);

            ProAssemText = itemView.findViewById(R.id.ProdctAssemText);
            ProdQtyText = itemView.findViewById(R.id.ProdcQtyText);
            ProPrzText = itemView.findViewById(R.id.ProdcPriceText);

            final View parent = itemView;

            AppDatabase db = AppDatabase.getAppDatabase(parent.getContext());
            final RecyclerPositionDao ReDao = db.recyclerPositionDao();
            final RecyclerPosition recyclerPosition = new RecyclerPosition(0);

        }

        @Override
        public void onClick(View v) {
            adapterposition = getAdapterPosition();

        }



        public void bind(ProductForAssem productForAssem) {

            ProAssemText.setText(productForAssem.getProducto());
            ProdQtyText.setText(productForAssem.getCantidad());
            ProPrzText.setText(productForAssem.getCosto());

            id = productForAssem.getProductoId();

        }



    }


    private List<ProductForAssem> productForAssems;

    public ProductsAdapter(Context context, List<ProductForAssem> productForAssems) {
        this.context = context;
        this.productForAssems = productForAssems;
    }

    public Integer getIdProduct(int position) {
        ProductForAssem productForAssem = new ProductForAssem("0",0,"0","0");
        productForAssem = productForAssems.get(position);
        return productForAssem.getProductoId();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.assemblie_details_item, viewGroup, false);
        ((Activity)viewGroup.getContext()).registerForContextMenu(view);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.bind(productForAssems.get(i));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Product_details.class);
                intent.putExtra(Product_details.CURRENT_PRODUCT_INDEX_EXTRA, viewHolder.id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productForAssems.size();
    }




}

public class ProductActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    private EditText Product;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());;
        Product_CategoriesDao CatDao = db.product_CategoriesDao();
        ProductsDao PrDao = db.productsDao();

        List<Products> products = PrDao.getAllProducts();

        List<ProductForAssem> productForAssems = new ArrayList<>();

        for (int i = 0; i < products.size(); i++){
            double precio =(products.get(i).getPrice())/100;
            ProductForAssem productForAssem = new ProductForAssem("0",0,"0","0");
            productForAssem.setCantidad(String.valueOf(products.get(i).getQty()));
            productForAssem.setCosto("$" + String.valueOf(obtieneDosDecimales(precio)));
            productForAssem.setProducto(products.get(i).getDescription());
            productForAssem.setProductoId(products.get(i).getId());
            productForAssems.add(productForAssem);
        }

        recyclerView = findViewById(R.id.recycler_view7);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ProductsAdapter(this,productForAssems));

        spinner = findViewById(R.id.CategoryProductSpinner);

        List<Product_categories> categories = CatDao.getAllProCategories();
        ArrayAdapter<String> categoriesadapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);

        categoriesadapter.add("Todos");

        for (int i = 0; i < categories.size(); i++) {
            categoriesadapter.add(categories.get(i).getDescription());
        }

        spinner.setAdapter(categoriesadapter);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.orders_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Spinner CategorySpinner;
        EditText ProSearchTxt;

        CategorySpinner = findViewById(R.id.CategoryProductSpinner);
        ProSearchTxt = findViewById(R.id.ProductSearchText);

        int category = 0;
        String Des = ProSearchTxt.getText().toString();

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        ProductsDao PrDao = db.productsDao();
        Product_CategoriesDao CatDao = db.product_CategoriesDao();

        category = CategorySpinner.getSelectedItemPosition();


        switch (item.getItemId()) {
            case R.id.search_menu_item:

                    if(Des == ""){
                        if (category == 0){
                            Toast.makeText(this, "No hay parametros de busqueda", Toast.LENGTH_SHORT).show();
                            List<Products> products = PrDao.getAllProducts();
                            ActRecy(products);
                        } else {
                            Toast.makeText(this, "Searching...", Toast.LENGTH_SHORT).show();
                            category = category - 1;
                            List<Products> products = PrDao.getProductsByCategory(category);
                            ActRecy(products);
                        }
                    }else {
                        if (category == 0){
                            Toast.makeText(this, "Searching...", Toast.LENGTH_SHORT).show();
                            String Text = "%" + Des + "%";
                            List<Products> products = PrDao.getProductsByDescription(Text);
                            ActRecy(products);
                        } else {
                            Toast.makeText(this, "Searching...", Toast.LENGTH_SHORT).show();
                            category = category - 1;
                            String Text = "%" + Des + "%";
                            List<Products> products = PrDao.getProductsByDescriptionAndCategory(Text , category);
                            ActRecy(products);
                        }
                    }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(24)
    private String obtieneDosDecimales(double valor){
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        return format.format(valor);
    }

    public void ActRecy(List<Products> products){

        List<ProductForAssem> productForAssems = new ArrayList<>();

        for (int i = 0; i < products.size(); i++){
            double precio =(products.get(i).getPrice())/100;
            ProductForAssem productForAssem = new ProductForAssem("0",0,"0","0");
            productForAssem.setCantidad(String.valueOf(products.get(i).getQty()));
            productForAssem.setCosto("$" + String.valueOf(obtieneDosDecimales(precio)));
            productForAssem.setProducto(products.get(i).getDescription());
            productForAssem.setProductoId(products.get(i).getId());
            productForAssems.add(productForAssem);
        }

        recyclerView = findViewById(R.id.recycler_view7);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ProductsAdapter(this,productForAssems));
    }
}
