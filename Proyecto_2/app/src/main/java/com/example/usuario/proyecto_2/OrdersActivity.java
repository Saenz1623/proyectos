package com.example.usuario.proyecto_2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.Assembly_products;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.Customers;
import com.example.usuario.proyecto_2.db.CustomersDao;
import com.example.usuario.proyecto_2.db.New_Order_Table;
import com.example.usuario.proyecto_2.db.New_Order_tableDao;
import com.example.usuario.proyecto_2.db.OrdenesRecy;
import com.example.usuario.proyecto_2.db.OrdenesRecyDao;
import com.example.usuario.proyecto_2.db.Order_assemblies;
import com.example.usuario.proyecto_2.db.Order_assembliesDao;
import com.example.usuario.proyecto_2.db.Order_status;
import com.example.usuario.proyecto_2.db.Order_statusDao;
import com.example.usuario.proyecto_2.db.Orders;
import com.example.usuario.proyecto_2.db.OrdersDao;
import com.example.usuario.proyecto_2.db.Products;
import com.example.usuario.proyecto_2.db.ProductsDao;
import com.example.usuario.proyecto_2.db.RecyclerPosition;
import com.example.usuario.proyecto_2.db.RecyclerPositionDao;
import com.facebook.stetho.Stetho;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder>{

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView ClientText;
        private TextView StatusText;
        private TextView DateText;
        private TextView QtyText;
        private TextView PrzText;

       private OrdenesRecy ordenesRecy;

        public ViewHolder (@NonNull final View itemView){
            super(itemView);

            ClientText = itemView.findViewById(R.id.ClientText);
            StatusText = itemView.findViewById(R.id.StatusText);
            DateText = itemView.findViewById(R.id.DateText);
            QtyText = itemView.findViewById(R.id.QtyText);
            PrzText = itemView.findViewById(R.id.PrzText);

            final View parent = itemView;

            AppDatabase db = AppDatabase.getAppDatabase(parent.getContext());
            final RecyclerPositionDao ReDao = db.recyclerPositionDao();
            final RecyclerPosition recyclerPosition = new RecyclerPosition(0);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // get position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if(pos != RecyclerView.NO_POSITION){
                        ReDao.nukeTable();
                        recyclerPosition.setPosition(pos);
                        ReDao.InsertPosition(recyclerPosition);
                    }
                    return false;
                }
            });


        }

        @Override
        public void onClick(View v) {
            int adapterposition;
            adapterposition = getAdapterPosition();

        }

        public void bind(OrdenesRecy ordenesRecy) {

            ClientText.setText(ordenesRecy.getCliente());
            StatusText.setText(ordenesRecy.getEstado());
            DateText.setText(ordenesRecy.getFecha());
            QtyText.setText(ordenesRecy.getQty());
            PrzText.setText(ordenesRecy.getCosto());
        }



    }

    private List<OrdenesRecy> ordenesRecies;

    public OrdersAdapter(List<OrdenesRecy> ordenesRecies) {
        this.ordenesRecies = ordenesRecies;
    }

    public OrdenesRecy getItem(int position) {
        return ordenesRecies.get(position);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.orders_item, viewGroup, false);
        ((Activity)viewGroup.getContext()).registerForContextMenu(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(ordenesRecies.get(i));
    }

    @Override
    public int getItemCount() {
        return ordenesRecies.size();
    }
}

public class OrdersActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private ImageButton addButton;

    private Spinner clientspinner;
    private Spinner statusspinner;

    private CheckBox inDateBox;
    private CheckBox FinDateBox;

    private Button InButton;
    private Button FinButton;

    Calendar cr;
    DatePickerDialog Dpd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Stetho.initializeWithDefaults(this);

        InButton = findViewById(R.id.ButtonInDate);
        FinButton = findViewById(R.id.ButtonFinDate);
        addButton = findViewById(R.id.AddButton);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        CustomersDao CuDao = db.customersDao();
        Order_statusDao StaDao = db.Order_statusDao();
        Order_assembliesDao OrAsDao = db.Order_assembliesDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        OrdenesRecyDao OrReDao = db.ordenesRecyDao();
        OrReDao.nukeTable();


        if (savedInstanceState != null) {
            InButton.setText(savedInstanceState.getString("IN_KEY", ""));
            FinButton.setText(savedInstanceState.getString("FIN_KEY", ""));
        }


        List<Orders> orders = OrDao.GetAllOrders();
        List<Customers> customers = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            customers.add(CuDao.getCustomersById(orders.get(i).getCustomer_id()));
        }
        List<Order_status> order_statuses = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            order_statuses.add(StaDao.getOrStatusById((orders.get(i).getStatus_id())));
        }
        List<String> Qty = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            Qty.add(String.valueOf(OrAsDao.getNumAssemById((orders.get(i).getId()))));
        }
        List<String> Price = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            List<Integer> orden = new ArrayList<>();
            List<Integer> qtyAs = new ArrayList<>();
            List<Integer> productos = new ArrayList<>();
            List<Integer> qtyPr = new ArrayList<>();
            double Precio = 0;
            double temp;
            double temp2 = 0;
            orden = OrAsDao.getAssemById(orders.get(i).getId());
            qtyAs = OrAsDao.getAssemQtyById(orders.get(i).getId());
            for (int g = 0; g < orden.size(); g++) {
                productos = AsprDao.getProductsById(orden.get(g));
                qtyPr = AsprDao.getProductsQtyById(orden.get(g));
                for (int j = 0; j < productos.size(); j++) {
                    temp = PrDao.getProductsPriceById(productos.get(j)) / 100;
                    temp = temp * qtyPr.get(j);
                    temp2 = temp2 + temp;
                }
                temp2 = temp2 * qtyAs.get(g);
                Precio = Precio + temp2;
                temp2 = 0;

            }

            Price.add("$" + obtieneDosDecimales(Precio));
            Precio = 0;
            orden.clear();
            qtyAs.clear();
            qtyPr.clear();
            productos.clear();
        }

        OrdenesRecy ordenesRecy = new OrdenesRecy(200, "", "", "", "", "");
        List<OrdenesRecy> ordenesRecyList = new ArrayList<>();

        for (int i = 0; i < orders.size(); i++) {
            ordenesRecy.setOrdRec_id(orders.get(i).getId());
            ordenesRecy.setCliente(customers.get(i).getFirst_name() + " " + customers.get(i).getLast_name());
            ordenesRecy.setCosto(Price.get(i));
            ordenesRecy.setFecha(orders.get(i).getDate());
            ordenesRecy.setEstado(order_statuses.get(i).getDescription());
            ordenesRecy.setQty(Qty.get(i));
            OrReDao.InsertOrden(ordenesRecy);
        }

        ordenesRecyList = OrReDao.getAll();

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new OrdersAdapter(ordenesRecyList));

        clientspinner = findViewById(R.id.ClientSpinner);
        statusspinner = findViewById(R.id.OrdenSpinner);
        inDateBox = findViewById(R.id.InDateCheckbox);
        FinDateBox = findViewById(R.id.FinDateCheckbox);
        InButton.findViewById(R.id.ButtonInDate);
        FinButton.findViewById(R.id.ButtonFinDate);

        InButton.setEnabled(false);
        FinButton.setEnabled(false);

        List<Customers> Clientes = CuDao.getAllCustomers();
        ArrayAdapter<String> Clientadapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);

        Clientadapter.add("Todos");

        for (int i = 0; i < Clientes.size(); i++) {
            Clientadapter.add(Clientes.get(i).getFirst_name() + " " + Clientes.get(i).getLast_name());
        }

        clientspinner.setAdapter(Clientadapter);

        inDateBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (inDateBox.isChecked()) {
                    InButton.setEnabled(true);
                }
                if (!inDateBox.isChecked()) {
                    InButton.setEnabled(false);
                }
            }
        });

        FinDateBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (FinDateBox.isChecked()) {
                    FinButton.setEnabled(true);
                }
                if (!FinDateBox.isChecked()) {
                    FinButton.setEnabled(false);
                }
            }
        });

        InButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cr = Calendar.getInstance();
                int day = cr.get(Calendar.DAY_OF_MONTH);
                int month = cr.get(Calendar.MONTH);
                int year = cr.get(Calendar.YEAR);

                Dpd = new DatePickerDialog(OrdersActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        InButton.setText(mDay + "-" + (mMonth + 1) + "-" + mYear);
                    }
                }, year, month, day);
                Dpd.show();
            }
        });

        FinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cr = Calendar.getInstance();
                int day = cr.get(Calendar.DAY_OF_MONTH);
                int month = cr.get(Calendar.MONTH);
                int year = cr.get(Calendar.YEAR);

                Dpd = new DatePickerDialog(OrdersActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        FinButton.setText(mDay + "-" + (mMonth + 1) + "-" + mYear);
                    }
                }, year, month, day);
                Dpd.show();
            }
        });

        ArrayList<StateVO> listVOs = new ArrayList<>();
        List<Order_status> order_statuses1 = StaDao.getAllOrStatus();
        for (int i = 0; i < order_statuses1.size(); i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(order_statuses1.get(i).getDescription());
            stateVO.setSelected(false);
            listVOs.add(stateVO);
        }

        MyAdapter myAdapter = new MyAdapter(OrdersActivity.this, 0,
                listVOs);
        statusspinner.setAdapter(myAdapter);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersActivity.this, NewOrderActivity.class);
                startActivityForResult(intent, NewOrderActivity.NEWASSEMBLIE_REQUEST_CODE);
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("IN_KEY", InButton.getText().toString());
        outState.putString("FIN_KEY", FinButton.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.orders_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Spinner ClientSpinner;
        Spinner StatusSpinner;

        ClientSpinner = findViewById(R.id.ClientSpinner);
        StatusSpinner = findViewById(R.id.OrdenSpinner);

        int StaFlag = 0;
        int customer = 0;
        int customertemp = 0;

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        Order_statusDao StaDao = db.Order_statusDao();
        CustomersDao CuDao = db.customersDao();

        customer = ClientSpinner.getSelectedItemPosition();

        List<Order_status> order_statuses = StaDao.getAllOrStatus();

        List<Integer> Statuses = new ArrayList<>();

        for(int i = 0; i < order_statuses.size(); i++) {
            StateVO stateVO = new StateVO();
            stateVO = (StateVO)StatusSpinner.getAdapter().getItem(i);
            if (stateVO.isSelected()){
                StaFlag = 1;
                Statuses.add(i);
            }
        }

        switch (item.getItemId()) {
            case R.id.search_menu_item:
                if (StaFlag == 0){
                    Toast.makeText(this, "No hay parametros de busqueda", Toast.LENGTH_SHORT).show();
                    List<Orders> orders= OrDao.GetAllOrders();
                    ActRecy(orders);
                }else {
                    Toast.makeText(this, "Searching...", Toast.LENGTH_SHORT).show();
                    List<Orders> orders = new ArrayList<>();
                    List<Orders> orderstemp = new ArrayList<>();
                    if (customer == 0) {
                        for (int i = 0; i < Statuses.size(); i++) {
                            orderstemp = OrDao.getOrdersByStatus(Statuses.get(i));
                            for (int g = 0; g < orderstemp.size(); g++) {
                                orders.add(orderstemp.get(g));
                            }
                            orderstemp.clear();
                        }
                        ActRecy(orders);
                    } else {
                        customertemp = customer - 1;

                        List<Customers> customers = CuDao.getAllCustomers();
                        int Customertemp2 = customertemp;
                        while (customers.get(Customertemp2).getId() != customertemp){
                            customertemp ++;
                        }

                        for (int i = 0; i < Statuses.size(); i++) {
                            orderstemp = OrDao.getOrdersByStatusandCustomer(Statuses.get(i), customertemp);
                            for (int g = 0; g < orderstemp.size(); g++) {
                                orders.add(orderstemp.get(g));
                            }
                            orderstemp.clear();
                        }
                        ActRecy(orders);
                    }

                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(24)
    private String obtieneDosDecimales(double valor) {
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        return format.format(valor);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        CustomersDao CuDao = db.customersDao();
        Order_statusDao StaDao = db.Order_statusDao();
        Order_assembliesDao OrAsDao = db.Order_assembliesDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        OrdenesRecyDao OrReDao = db.ordenesRecyDao();
        OrReDao.nukeTable();

        List<Orders> orders = OrDao.GetAllOrders();
        List<Customers> customers = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            customers.add(CuDao.getCustomersById(orders.get(i).getCustomer_id()));
        }
        List<Order_status> order_statuses = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            order_statuses.add(StaDao.getOrStatusById((orders.get(i).getStatus_id())));
        }
        List<String> Qty = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            Qty.add(String.valueOf(OrAsDao.getNumAssemById((orders.get(i).getId()))));
        }
        List<String> Price = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            List<Integer> orden = new ArrayList<>();
            List<Integer> qtyAs = new ArrayList<>();
            List<Integer> productos = new ArrayList<>();
            List<Integer> qtyPr = new ArrayList<>();
            double Precio = 0;
            double temp;
            double temp2 = 0;
            orden = OrAsDao.getAssemById(orders.get(i).getId());
            qtyAs = OrAsDao.getAssemQtyById(orders.get(i).getId());
            for (int g = 0; g < orden.size(); g++) {
                productos = AsprDao.getProductsById(orden.get(g));
                qtyPr = AsprDao.getProductsQtyById(orden.get(g));
                for (int j = 0; j < productos.size(); j++) {
                    temp = PrDao.getProductsPriceById(productos.get(j)) / 100;
                    temp = temp * qtyPr.get(j);
                    temp2 = temp2 + temp;
                }
                temp2 = temp2 * qtyAs.get(g);
                Precio = Precio + temp2;
                temp2 = 0;

            }

            Price.add("$" + obtieneDosDecimales(Precio));
            Precio = 0;
            orden.clear();
            qtyAs.clear();
            qtyPr.clear();
            productos.clear();
        }

        OrdenesRecy ordenesRecy = new OrdenesRecy(200, "", "", "", "", "");
        List<OrdenesRecy> ordenesRecyList = new ArrayList<>();

        for (int i = 0; i < orders.size(); i++) {
            ordenesRecy.setOrdRec_id(orders.get(i).getId());
            ordenesRecy.setCliente(customers.get(i).getFirst_name() + " " + customers.get(i).getLast_name());
            ordenesRecy.setCosto(Price.get(i));
            ordenesRecy.setFecha(orders.get(i).getDate());
            ordenesRecy.setEstado(order_statuses.get(i).getDescription());
            ordenesRecy.setQty(Qty.get(i));
            OrReDao.InsertOrden(ordenesRecy);
        }

        ordenesRecyList = OrReDao.getAll();


        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case NewAssemblie.NEWASSEMBLIE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    recyclerView = findViewById(R.id.recycler_view);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(new OrdersAdapter(ordenesRecyList));
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

            case EditOrderActivity.EDITORDER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    recyclerView = findViewById(R.id.recycler_view);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(new OrdersAdapter(ordenesRecyList));
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

            default:
                // other activities...
                break;
        }
    }

    public void ActRecy(List<Orders> orders){
        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        CustomersDao CuDao = db.customersDao();
        Order_statusDao StaDao = db.Order_statusDao();
        Order_assembliesDao OrAsDao = db.Order_assembliesDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        OrdenesRecyDao OrReDao = db.ordenesRecyDao();
        OrReDao.nukeTable();

        List<Customers> customers = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            customers.add(CuDao.getCustomersById(orders.get(i).getCustomer_id()));
        }
        List<Order_status> order_statuses = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            order_statuses.add(StaDao.getOrStatusById((orders.get(i).getStatus_id())));
        }
        List<String> Qty = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            Qty.add(String.valueOf(OrAsDao.getNumAssemById((orders.get(i).getId()))));
        }
        List<String> Price = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            List<Integer> orden = new ArrayList<>();
            List<Integer> qtyAs = new ArrayList<>();
            List<Integer> productos = new ArrayList<>();
            List<Integer> qtyPr = new ArrayList<>();
            double Precio = 0;
            double temp;
            double temp2 = 0;
            orden = OrAsDao.getAssemById(orders.get(i).getId());
            qtyAs = OrAsDao.getAssemQtyById(orders.get(i).getId());
            for (int g = 0; g < orden.size(); g++) {
                productos = AsprDao.getProductsById(orden.get(g));
                qtyPr = AsprDao.getProductsQtyById(orden.get(g));
                for (int j = 0; j < productos.size(); j++) {
                    temp = PrDao.getProductsPriceById(productos.get(j)) / 100;
                    temp = temp * qtyPr.get(j);
                    temp2 = temp2 + temp;
                }
                temp2 = temp2 * qtyAs.get(g);
                Precio = Precio + temp2;
                temp2 = 0;

            }

            Price.add("$" + obtieneDosDecimales(Precio));
            Precio = 0;
            orden.clear();
            qtyAs.clear();
            qtyPr.clear();
            productos.clear();
        }

        OrdenesRecy ordenesRecy = new OrdenesRecy(200, "", "", "", "", "");
        List<OrdenesRecy> ordenesRecyList = new ArrayList<>();

        for (int i = 0; i < orders.size(); i++) {
            ordenesRecy.setOrdRec_id(orders.get(i).getId());
            ordenesRecy.setCliente(customers.get(i).getFirst_name() + " " + customers.get(i).getLast_name());
            ordenesRecy.setCosto(Price.get(i));
            ordenesRecy.setFecha(orders.get(i).getDate());
            ordenesRecy.setEstado(order_statuses.get(i).getDescription());
            ordenesRecy.setQty(Qty.get(i));
            OrReDao.InsertOrden(ordenesRecy);
        }

        ordenesRecyList = OrReDao.getAll();

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new OrdersAdapter(ordenesRecyList));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.orderact_menu, menu);

        RecyclerView recyclerView;

        recyclerView = findViewById(R.id.recycler_view);



        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        RecyclerPositionDao ReDao = db.recyclerPositionDao();
        OrdersDao OrDao = db.OrdersDao();
        OrdenesRecyDao RecyDao = db.ordenesRecyDao();

        OrdersAdapter ordersAdapter = (OrdersAdapter) recyclerView.getAdapter();
        int position = ReDao.getposition();

        List<OrdenesRecy> ordenesRecyList = RecyDao.getAll();
        OrdenesRecy ordenesRecy = ordenesRecyList.get(position);

        Orders orders = OrDao.getOrdersById(ordenesRecy.getOrdRec_id());
        int status = orders.getStatus_id();

        if(status >= 2){
            menu.setGroupVisible(R.id.Editar_orden_group, false);
            menu.setGroupVisible(R.id.Retroceder_orden_group, false);
            if (status >= 4){
                menu.setGroupVisible(R.id.Avanzar_orden_group, false);
                               }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final RecyclerView recyclerView;

        recyclerView = findViewById(R.id.recycler_view);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        RecyclerPositionDao ReDao = db.recyclerPositionDao();
        OrdersDao OrDao = db.OrdersDao();
        OrdenesRecyDao RecyDao = db.ordenesRecyDao();
        Order_statusDao StaDao = db.Order_statusDao();



        OrdersAdapter ordersAdapter = (OrdersAdapter) recyclerView.getAdapter();
        int position = ReDao.getposition();

        List<OrdenesRecy> ordenesRecyList = RecyDao.getAll();
        OrdenesRecy ordenesRecy = ordenesRecyList.get(position);

        Orders orders = OrDao.getOrdersById(ordenesRecy.getOrdRec_id());
        int status = orders.getStatus_id();

        switch (item.getItemId()) {

            case R.id.DetailsOrder_item:
                OrdenesRecy ordenesRecy2 = ordenesRecyList.get(position);
                Intent intent = new Intent(OrdersActivity.this, OrderDetailsActivity.class);
                intent.putExtra(OrderDetailsActivity.CURRENT_ORDER_INDEX_EXTRA, ordenesRecy2.getOrdRec_id());
                startActivity(intent);
                return true;

            case R.id.Retroceder_orden_item:
                createCustomDialogRetro().show();
                return true;

            case R.id.Avanzar_orden_item:
                createCustomDialogAvan().show();
                return true;

            case R.id.Editar_orden_item:
                OrdenesRecy ordenesRecy3 = ordenesRecyList.get(position);
                Intent intent2 = new Intent(OrdersActivity.this, EditOrderActivity.class);
                intent2.putExtra(EditOrderActivity.CURRENT_ORDER_INDEX_EXTRA, ordenesRecy3.getOrdRec_id());
                startActivityForResult(intent2, EditOrderActivity.EDITORDER_REQUEST_CODE);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    public AlertDialog createCustomDialogAvan() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();

        View v = inflater.inflate(R.layout.avanzarstatusdialog, null);
        //builder.setView(inflater.inflate(R.layout.dialog_signin, null))
        Button AddStatus;
        Button CancelStatus;

        TextView Statustext;

        final EditText ComentText;

        AddStatus = v.findViewById(R.id.SaveStatus);
        CancelStatus = v.findViewById(R.id.CancelStatus);

        Statustext = v.findViewById(R.id.StatusText);

        ComentText = v.findViewById(R.id.ComentText);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        RecyclerPositionDao ReDao = db.recyclerPositionDao();
        final OrdersDao OrDao = db.OrdersDao();
        final OrdenesRecyDao RecyDao = db.ordenesRecyDao();
        Order_statusDao StaDao = db.Order_statusDao();



        int position = ReDao.getposition();

        List<OrdenesRecy> ordenesRecyList = RecyDao.getAll();
        OrdenesRecy ordenesRecy = ordenesRecyList.get(position);

        final Orders orders = OrDao.getOrdersById(ordenesRecy.getOrdRec_id());
        int status = orders.getStatus_id();

        final Order_status order_status = StaDao.getOrStatusById(status);

        int StatusNext = 0;

        if (order_status.getId() == 0) {
            StatusNext = 2;
        }
        if (order_status.getId() == 1) {
            StatusNext = 0;
        }
        if (order_status.getId() == 2) {
            StatusNext = 3;
        }
        if (order_status.getId() == 3) {
            StatusNext = 4;
        }

        final Order_status order_status1 = StaDao.getOrStatusById(StatusNext);


        Statustext.setText("Avanzar estado a: " + order_status1.getDescription());


        builder.setView(v);
        alertDialog = builder.create();

        AddStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        Calendar cr;
                        cr = Calendar.getInstance();
                        final int day = cr.get(Calendar.DAY_OF_MONTH);
                        final int month = cr.get(Calendar.MONTH);
                        final int year = cr.get(Calendar.YEAR);
                        String Cambios = ComentText.getText().toString();
                        orders.setStatus_id(order_status1.getId());
                        orders.setDate(String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year));
                        orders.setChange_log(orders.getDate() + "\n Se Avanzo la orden de " +
                                order_status.getDescription() + " a " + order_status1.getDescription() + "\n" + Cambios + "\n" + orders.getChange_log());
                        OrDao.UpdateOrder(orders);
                        List<Orders> orders1 = OrDao.GetAllOrders();
                        ActRecy(orders1);
                        Toast.makeText(getApplicationContext(), "Se Actualizo la orden", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                }
        );
        CancelStatus.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Se cancelo la operación", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                }
        );
        return alertDialog;
    }

    public AlertDialog createCustomDialogRetro() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();

        View v = inflater.inflate(R.layout.avanzarstatusdialog, null);
        //builder.setView(inflater.inflate(R.layout.dialog_signin, null))
        Button AddStatus;
        Button CancelStatus;

        TextView Statustext;

        final EditText ComentText;

        AddStatus = v.findViewById(R.id.SaveStatus);
        CancelStatus = v.findViewById(R.id.CancelStatus);

        Statustext = v.findViewById(R.id.StatusText);

        ComentText = v.findViewById(R.id.ComentText);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        RecyclerPositionDao ReDao = db.recyclerPositionDao();
        final OrdersDao OrDao = db.OrdersDao();
        final OrdenesRecyDao RecyDao = db.ordenesRecyDao();
        Order_statusDao StaDao = db.Order_statusDao();


        int position = ReDao.getposition();

        List<OrdenesRecy> ordenesRecyList = RecyDao.getAll();
        OrdenesRecy ordenesRecy = ordenesRecyList.get(position);

        final Orders orders = OrDao.getOrdersById(ordenesRecy.getOrdRec_id());
        int status = orders.getStatus_id();

        final Order_status order_status = StaDao.getOrStatusById(status);

        int StatusNext = 0;

        if (order_status.getId() == 0) {
            StatusNext = 1;
        }
        if (order_status.getId() == 1) {
            StatusNext = 1;
        }

        final Order_status order_status1 = StaDao.getOrStatusById(StatusNext);


        Statustext.setText("Retroceder estado a: " + order_status1.getDescription());


        builder.setView(v);
        alertDialog = builder.create();

        AddStatus.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar cr;
                        cr = Calendar.getInstance();
                        final int day = cr.get(Calendar.DAY_OF_MONTH);
                        final int month = cr.get(Calendar.MONTH);
                        final int year = cr.get(Calendar.YEAR);
                        String Cambios = ComentText.getText().toString();
                        orders.setStatus_id(order_status1.getId());
                        orders.setDate(String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year));
                        orders.setChange_log(orders.getDate() + "\n Se Retrocedio la orden de " +
                                order_status.getDescription() + " a " + order_status1.getDescription() + "\n" + Cambios + "\n" + orders.getChange_log());
                        OrDao.UpdateOrder(orders);
                        List<Orders> orders1 = OrDao.GetAllOrders();
                        ActRecy(orders1);
                        Toast.makeText(getApplicationContext(), "Se Actualizo la orden", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                }
        );
        CancelStatus.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Se cancelo la operación", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                }
        );
        return alertDialog;
    }


}
