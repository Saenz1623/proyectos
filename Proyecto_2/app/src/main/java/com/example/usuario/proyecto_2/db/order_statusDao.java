package com.example.usuario.proyecto_2.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Order_statusDao {

    @Insert
    public void InsertOrderStatus(Order_status order_status);

    @Update
    public void UpdateOrderStatus(Order_status order_status);

    @Query("SELECT * FROM order_status ORDER BY id")
    public List<Order_status> getAllOrStatus();

    @Query("SELECT * FROM order_status WHERE id = :id")
    public Order_status getOrStatusById(int id);

    @Query("SELECT id FROM order_status WHERE id = :id")
    public Integer getOneOrStatusById(int id);

    @Query("SELECT description FROM order_status WHERE id = :id")
    public String getOneOrStatusdesById(int id);


}
