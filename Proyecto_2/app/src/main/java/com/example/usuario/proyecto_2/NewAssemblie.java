package com.example.usuario.proyecto_2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.Assemblies;
import com.example.usuario.proyecto_2.db.AssembliesDao;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.CustomersDao;
import com.example.usuario.proyecto_2.db.New_Order_Table;
import com.example.usuario.proyecto_2.db.New_Order_tableDao;
import com.example.usuario.proyecto_2.db.OrdenesRecy;
import com.example.usuario.proyecto_2.db.Order_assembliesDao;
import com.example.usuario.proyecto_2.db.Orders;
import com.example.usuario.proyecto_2.db.OrdersDao;
import com.example.usuario.proyecto_2.db.ProductsDao;
import com.example.usuario.proyecto_2.db.RecyclerPosition;
import com.example.usuario.proyecto_2.db.RecyclerPositionDao;

import java.util.ArrayList;
import java.util.List;



class NewAssemblieAdapter extends RecyclerView.Adapter<NewAssemblieAdapter.ViewHolder> {

    public int position;


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView OrAssemText;
        private TextView ProQtyText;
        private TextView AssemPrzText;
        private int adapterposition;

        private New_Order_Table new_order_table;


        public ViewHolder (@NonNull final View itemView) {
            super(itemView);

            OrAssemText = itemView.findViewById(R.id.OrAssemText);
            ProQtyText = itemView.findViewById(R.id.ProQtyText);
            AssemPrzText = itemView.findViewById(R.id.AssemPrzText);

            final View parent = itemView;

            AppDatabase db = AppDatabase.getAppDatabase(parent.getContext());
            final RecyclerPositionDao ReDao = db.recyclerPositionDao();
            final RecyclerPosition recyclerPosition = new RecyclerPosition(0);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // get position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if(pos != RecyclerView.NO_POSITION){
                        ReDao.nukeTable();
                        recyclerPosition.setPosition(pos);
                        ReDao.InsertPosition(recyclerPosition);
                    }
                    return false;
                }
            });



        }

        @Override
        public void onClick(View v) {
            adapterposition = getAdapterPosition();

        }



        public void bind(New_Order_Table new_order_table) {

            OrAssemText.setText(new_order_table.getAssembly());
            ProQtyText.setText(new_order_table.getQtyProducs());
            AssemPrzText.setText(new_order_table.getPrice());
        }



    }


    private List<New_Order_Table> new_order_tables;



    public NewAssemblieAdapter(List<New_Order_Table> new_order_tables) {
        this.new_order_tables = new_order_tables;
    }

    public New_Order_Table getItem(int position) {
        return new_order_tables.get(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.assem_item, viewGroup, false);
        ((Activity)viewGroup.getContext()).registerForContextMenu(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(new_order_tables.get(i));
    }

    @Override
    public int getItemCount() {
        return new_order_tables.size();
    }




}

public class NewAssemblie extends AppCompatActivity {

    private EditText AssemSeText;
    private RecyclerView recyclerView;
    public static final int NEWASSEMBLIE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_assemblie);

        Toolbar toolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);

        AssemSeText = findViewById(R.id.AssemSearchText);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        New_Order_tableDao NewOrDao = db.new_order_tableDao();
        AssembliesDao AsDao = db.AssembliesDao();

        List<Assemblies> assemblies = AsDao.getAllassemblies();
        List<Orders> orders = OrDao.GetAllOrders();
        int temp1 = 0;
        for (int i = 0; i < orders.size(); i++){
            temp1 = temp1 + 1;
        }
        int ordenes = temp1;
        List<New_Order_Table> new_order_tables = NewOrDao.getAll();
        int temp2 = 0;
        for (int i = 0; i < new_order_tables.size(); i++){
            temp2 = temp2 + 1;
        }
        int tabla = temp2;
        List<New_Order_Table> new_order_tables1 = new ArrayList<>();

        for (int i = 0; i < assemblies.size(); i++)
            {
                New_Order_Table new_order_table = new New_Order_Table(0,0,0, 0, "0","0","0");
                new_order_table.setOr_id(ordenes);
                new_order_table.setNewor_id(tabla);
                new_order_table.setAssembly_id(assemblies.get(i).getId());
                new_order_table.setAssembly(assemblies.get(i).getDescription());
                new_order_table.setQty(1);
                new_order_table.setQtyProducs(String.valueOf(AsprDao.getSumProductsQtyById(assemblies.get(i).getId())));

                List <Integer> productos = new ArrayList<>();
                List<Integer> qtyPr = new ArrayList<>();
                double Precio = 0;
                double temp;
                productos = AsprDao.getProductsById(assemblies.get(i).getId());
                qtyPr = AsprDao.getProductsQtyById(assemblies.get(i).getId());
                for (int j = 0; j < productos.size(); j++){
                        temp = PrDao.getProductsPriceById(productos.get(j))/100;
                        temp = temp * qtyPr.get(j);
                        Precio = Precio + temp;
                    }

                new_order_table.setPrice("$" + obtieneDosDecimales(Precio));
                Precio = 0;
                qtyPr.clear();
                productos.clear();

                new_order_tables1.add(new_order_table);
            }

        recyclerView = findViewById(R.id.recycler_view3);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new NewAssemblieAdapter(new_order_tables1));



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.orders_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AssemSeText = findViewById(R.id.AssemSearchText);
        RecyclerView recyclerView;
       String Text;
       String TextSea;

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        New_Order_tableDao NewOrDao = db.new_order_tableDao();
        AssembliesDao AsDao = db.AssembliesDao();

        switch (item.getItemId()) {
            case R.id.search_menu_item:

                Text = AssemSeText.getText().toString();
                if ("".equals(Text)){
                    Toast.makeText(this, "No hay parametro de busqueda", Toast.LENGTH_SHORT).show();
                    List<Assemblies> assemblies = AsDao.getAllassemblies();
                    List<Orders> orders = OrDao.GetAllOrders();
                    int temp1 = 0;
                    for (int i = 0; i < orders.size(); i++){
                        temp1 = temp1 + 1;
                    }
                    int ordenes = temp1;
                    List<New_Order_Table> new_order_tables = NewOrDao.getAll();
                    int temp2 = 0;
                    for (int i = 0; i < new_order_tables.size(); i++){
                        temp2 = temp2 + 1;
                    }
                    int tabla = temp2;
                    List<New_Order_Table> new_order_tables1 = new ArrayList<>();

                    for (int i = 0; i < assemblies.size(); i++)
                    {
                        New_Order_Table new_order_table = new New_Order_Table(0,0,0, 0, "0","0","0");
                        new_order_table.setOr_id(ordenes);
                        new_order_table.setNewor_id(tabla);
                        new_order_table.setAssembly_id(assemblies.get(i).getId());
                        new_order_table.setAssembly(assemblies.get(i).getDescription());
                        new_order_table.setQty(1);
                        new_order_table.setQtyProducs(String.valueOf(AsprDao.getSumProductsQtyById(assemblies.get(i).getId())));

                        List <Integer> productos = new ArrayList<>();
                        List<Integer> qtyPr = new ArrayList<>();
                        double Precio = 0;
                        double temp;
                        productos = AsprDao.getProductsById(assemblies.get(i).getId());
                        qtyPr = AsprDao.getProductsQtyById(assemblies.get(i).getId());
                        for (int j = 0; j < productos.size(); j++){
                            temp = PrDao.getProductsPriceById(productos.get(j))/100;
                            temp = temp * qtyPr.get(j);
                            Precio = Precio + temp;
                        }

                        new_order_table.setPrice("$" + obtieneDosDecimales(Precio));
                        Precio = 0;
                        qtyPr.clear();
                        productos.clear();

                        new_order_tables1.add(new_order_table);
                    }

                    recyclerView = findViewById(R.id.recycler_view3);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(new NewAssemblieAdapter(new_order_tables1));
                } else {
                    TextSea = "%" + Text + "%";
                    Toast.makeText(this, "Buscando por: " + Text, Toast.LENGTH_SHORT).show();
                    List<Assemblies> assemblies = AsDao.getassembliesByDescription(TextSea);
                    List<Orders> orders = OrDao.GetAllOrders();
                    int temp1 = 0;
                    for (int i = 0; i < orders.size(); i++){
                        temp1 = temp1 + 1;
                    }
                    int ordenes = temp1;
                    List<New_Order_Table> new_order_tables = NewOrDao.getAll();
                    int temp2 = 0;
                    for (int i = 0; i < new_order_tables.size(); i++){
                        temp2 = temp2 + 1;
                    }
                    int tabla = temp2;
                    List<New_Order_Table> new_order_tables1 = new ArrayList<>();

                    for (int i = 0; i < assemblies.size(); i++)
                    {
                        New_Order_Table new_order_table = new New_Order_Table(0,0,0, 0, "0","0","0");
                        new_order_table.setOr_id(ordenes);
                        new_order_table.setNewor_id(tabla);
                        new_order_table.setAssembly_id(assemblies.get(i).getId());
                        new_order_table.setAssembly(assemblies.get(i).getDescription());
                        new_order_table.setQty(1);
                        new_order_table.setQtyProducs(String.valueOf(AsprDao.getSumProductsQtyById(assemblies.get(i).getId())));

                        List <Integer> productos = new ArrayList<>();
                        List<Integer> qtyPr = new ArrayList<>();
                        double Precio = 0;
                        double temp;
                        productos = AsprDao.getProductsById(assemblies.get(i).getId());
                        qtyPr = AsprDao.getProductsQtyById(assemblies.get(i).getId());
                        for (int j = 0; j < productos.size(); j++){
                            temp = PrDao.getProductsPriceById(productos.get(j))/100;
                            temp = temp * qtyPr.get(j);
                            Precio = Precio + temp;
                        }

                        new_order_table.setPrice("$" + obtieneDosDecimales(Precio));
                        Precio = 0;
                        qtyPr.clear();
                        productos.clear();

                        new_order_tables1.add(new_order_table);
                    }

                    recyclerView = findViewById(R.id.recycler_view3);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(new NewAssemblieAdapter(new_order_tables1));
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(24)
    private String obtieneDosDecimales(double valor){
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        return format.format(valor);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.assem_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        RecyclerView recyclerView;
        Spinner spinner;

        recyclerView = findViewById(R.id.recycler_view3);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        RecyclerPositionDao ReDao = db.recyclerPositionDao();
        final New_Order_tableDao NeDao = db.new_order_tableDao();
        final List<New_Order_Table> new_order_tables = NeDao.getAll();

        final New_Order_Table new_order_table;

        NewAssemblieAdapter newAssemblieAdapter = (NewAssemblieAdapter)recyclerView.getAdapter();
        int position = ReDao.getposition();

        switch (item.getItemId()) {

            case R.id.DetailsAssem_item:
                new_order_table = newAssemblieAdapter.getItem(position);
                Intent intent = new Intent(NewAssemblie.this, AssemblieDetails.class);
                intent.putExtra(AssemblieDetails.CURRENT_ASSEMBLIE_INDEX_EXTRA, new_order_table.getAssembly_id());
                startActivityForResult(intent, AssemblieDetails.ASSEMBLIE_REQUEST_CODE);
                return true;

            case R.id.AddAssem_item:
                new_order_table = newAssemblieAdapter.getItem(position);
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                dialogo1.setTitle("Importante");
                dialogo1.setMessage("¿Desea agregar el ensamble?" + " " + new_order_table.getAssembly());
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        int prueba = 0;
                        New_Order_Table new_order_table1;
                        for (int i = 0; i < new_order_tables.size(); i++){
                            if (new_order_table.getAssembly_id() == new_order_tables.get(i).getAssembly_id()){
                                prueba = 1;
                            }
                        }
                        if (prueba == 1){
                            new_order_table1 = NeDao.getOrderByAssemid(new_order_table.getAssembly_id());
                            new_order_table1.setQty(new_order_table1.getQty() + 1 );
                            NeDao.UpdateNewOrder(new_order_table1);
                            Toast t = Toast.makeText(getApplicationContext(), "Ya habia seleccionado este ensamble, añadiendole una cantidad", Toast.LENGTH_SHORT);
                            t.show();
                        }else {
                            NeDao.InsertOrden(new_order_table);
                            Toast t = Toast.makeText(getApplicationContext(), "Se ha agregado el ensamble correctamente", Toast.LENGTH_SHORT);
                            t.show();
                        }
                        setResult(RESULT_OK);
                        finish();
                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Toast t=Toast.makeText(getApplicationContext(),"Se ha cancelado la operación", Toast.LENGTH_SHORT);
                        t.show();
                }
                });
                dialogo1.show();

                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }



}
