package com.example.usuario.proyecto_2.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Assembly_productsDao {

    @Insert
    public void InsertAssemblies_products(Assembly_products assemblies_products);

    @Update
    public void UpdateAssemblies_products(Assembly_products assemblies_products);

    @Query("SELECT * FROM assembly_products ORDER BY id")
    public List<Assembly_products> getAllassemblies_products();

    @Query("SELECT * FROM assembly_products WHERE id = :id")
    public Assembly_products getProductAssembliesById(int id);

    @Query("SELECT product_id FROM assembly_products WHERE id = :id")
    public List<Integer> getProductsById(int id);

    @Query("SELECT qty FROM assembly_products WHERE id = :id")
    public List<Integer> getProductsQtyById(int id);

    @Query("SELECT SUM(qty) FROM assembly_products WHERE id = :id")
    public int getSumProductsQtyById(int id);



}
