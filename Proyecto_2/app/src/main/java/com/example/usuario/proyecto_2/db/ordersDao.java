package com.example.usuario.proyecto_2.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface OrdersDao {

    @Insert
    public void InsertOrder(Orders orders);

    @Update
    public void UpdateOrder(Orders orders);

    @Query("SELECT * FROM orders ORDER BY id")
    public List<Orders> GetAllOrders();

    @Query("SELECT * FROM orders WHERE id = :id")
    public Orders getOrdersById(int id);

    @Query("SELECT * FROM orders WHERE customer_id = :customer_id")
    public List<Orders> getOrdersByCustomers(int customer_id);

    @Query("SELECT * FROM orders WHERE status_id = :status_id")
    public List<Orders> getOrdersByStatus(int status_id);

    @Query("SELECT * FROM orders WHERE status_id = :status_id AND customer_id = :customer_id")
    public List<Orders> getOrdersByStatusandCustomer(int status_id, int customer_id);

    @Query("SELECT * FROM orders WHERE customer_id = :id")
    public List<Orders> getOrdersByCuId(int id);
}
