package com.example.usuario.proyecto_2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.AssembliesDao;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.Customers;
import com.example.usuario.proyecto_2.db.CustomersDao;
import com.example.usuario.proyecto_2.db.New_Order_Table;
import com.example.usuario.proyecto_2.db.New_Order_tableDao;
import com.example.usuario.proyecto_2.db.Order_assembliesDao;
import com.example.usuario.proyecto_2.db.Order_status;
import com.example.usuario.proyecto_2.db.Order_statusDao;
import com.example.usuario.proyecto_2.db.Orders;
import com.example.usuario.proyecto_2.db.OrdersDao;
import com.example.usuario.proyecto_2.db.ProductsDao;
import com.example.usuario.proyecto_2.db.RecyclerPosition;
import com.example.usuario.proyecto_2.db.RecyclerPositionDao;

import java.util.ArrayList;
import java.util.List;

class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.ViewHolder>{

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView AssemText;
        private TextView QtyText;
        private TextView PrzText;
        private TextView txtPoints;

        private New_Order_Table new_order_table;

        public ViewHolder (@NonNull final View itemView){
            super(itemView);

            AssemText = itemView.findViewById(R.id.OrDetAssemText);
            QtyText = itemView.findViewById(R.id.ProDetQtyText);
            PrzText = itemView.findViewById(R.id.AssemDetPrzText);
            txtPoints = itemView.findViewById(R.id.AssemQtyDetText);


            final View parent = itemView;



        }


        public void bind(New_Order_Table new_order_table) {

            this.new_order_table = new_order_table;

            AssemText.setText(new_order_table.getAssembly());;
            QtyText.setText(new_order_table.getQtyProducs());
            PrzText.setText(new_order_table.getPrice());
            txtPoints.setText(String.valueOf(new_order_table.getQty()));
        }



    }

    private List<New_Order_Table> new_order_tables;

    public OrderDetailsAdapter(List<New_Order_Table> new_order_tables) {
        this.new_order_tables = new_order_tables;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.order_details_item, viewGroup, false);
        ((Activity)viewGroup.getContext()).registerForContextMenu(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(new_order_tables.get(i));
    }

    @Override
    public int getItemCount() {
        return new_order_tables.size();
    }
}

public class OrderDetailsActivity extends AppCompatActivity {

    private TextView ClienteTxt;
    private TextView CambiosTxt;
    private TextView StatusTxt;

    RecyclerView recyclerView;

    public static final String CURRENT_ORDER_INDEX_EXTRA = "COM.EXAMPLE.NEWASSEMBLIE.CURRENT_ASSEMBLIE";

    private int CurrentOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        CurrentOrder = intent.getIntExtra(CURRENT_ORDER_INDEX_EXTRA,0);


        ClienteTxt = findViewById(R.id.ClienteOrDetailsText);
        CambiosTxt = findViewById(R.id.ChangeLog);
        StatusTxt = findViewById(R.id.StatusOrDetailsText);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        OrdersDao OrDao = db.OrdersDao();
        CustomersDao CuDao = db.customersDao();
        Order_statusDao StaDao = db.Order_statusDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        AssembliesDao AsDao = db.AssembliesDao();
        New_Order_tableDao NewOrDao = db.new_order_tableDao();
        Order_assembliesDao orderAssembliesDao = db.Order_assembliesDao();
        NewOrDao.nukeTable();

        Orders orders = OrDao.getOrdersById(CurrentOrder);

        Customers customer = CuDao.getCustomersById(orders.getCustomer_id());
        String customerName = customer.getFirst_name() + " " + customer.getLast_name();

        String Status = StaDao.getOneOrStatusdesById(orders.getStatus_id());

        ClienteTxt.setText("Cliente: " + customerName);
        StatusTxt.setText("Estado de la orden: " + Status);
        CambiosTxt.setText(orders.getChange_log());
        CambiosTxt.setMovementMethod(new ScrollingMovementMethod());

        List<Integer> Assemblies = orderAssembliesDao.getAssemById(CurrentOrder);

        for (int i = 0; i < Assemblies.size() ; i ++){
            New_Order_Table new_order_table = new New_Order_Table(0,0,0, 0, "0","0","0");
            new_order_table.setQty(orderAssembliesDao.getOneAssemQtyById(Assemblies.get(i), CurrentOrder));

            List <Integer> productos = new ArrayList<>();
            List<Integer> qtyPr = new ArrayList<>();
            double Precio = 0;
            double temp;
            productos = AsprDao.getProductsById(Assemblies.get(i));
            qtyPr = AsprDao.getProductsQtyById(Assemblies.get(i));
            for (int j = 0; j < productos.size(); j++){
                temp = PrDao.getProductsPriceById(productos.get(j))/100;
                temp = temp * qtyPr.get(j);
                Precio = Precio + temp;
            }

            new_order_table.setPrice("$" + obtieneDosDecimales(Precio));
            Precio = 0;
            qtyPr.clear();
            productos.clear();
            new_order_table.setQtyProducs(String.valueOf(AsprDao.getSumProductsQtyById(Assemblies.get(i))));
            new_order_table.setAssembly(AsDao.getAssembliesDesById(Assemblies.get(i)));
            new_order_table.setAssembly_id(Assemblies.get(i));
            new_order_table.setOr_id(CurrentOrder);
            new_order_table.setNewor_id(i);
            NewOrDao.InsertOrden(new_order_table);
        }


        List<New_Order_Table> new_order_tables = NewOrDao.getAll();

        recyclerView = findViewById(R.id.recycler_view5);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new OrderDetailsAdapter(new_order_tables));





    }

    @TargetApi(24)
    private String obtieneDosDecimales(double valor){
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        return format.format(valor);
    }

    @Override
    public void onBackPressed() {
        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        New_Order_tableDao NewOrDao = db.new_order_tableDao();
        NewOrDao.nukeTable();
        super.onBackPressed();
    }
}
