package com.example.usuario.proyecto_2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.AssembliesDao;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.New_Order_Table;
import com.example.usuario.proyecto_2.db.New_Order_tableDao;
import com.example.usuario.proyecto_2.db.OrdersDao;
import com.example.usuario.proyecto_2.db.ProductsDao;
import com.example.usuario.proyecto_2.db.RecyclerPosition;
import com.example.usuario.proyecto_2.db.RecyclerPositionDao;

import java.util.ArrayList;
import java.util.List;

class AssemblieDetailsAdapter extends RecyclerView.Adapter<AssemblieDetailsAdapter.ViewHolder> {

    private Context context;


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView ProAssemText;
        private TextView ProdQtyText;
        private TextView ProPrzText;
        private int adapterposition;

        private ProductForAssem productForAssem;

        private int id;


        public ViewHolder (@NonNull final View itemView) {
            super(itemView);

            ProAssemText = itemView.findViewById(R.id.ProdctAssemText);
            ProdQtyText = itemView.findViewById(R.id.ProdcQtyText);
            ProPrzText = itemView.findViewById(R.id.ProdcPriceText);

            final View parent = itemView;

            AppDatabase db = AppDatabase.getAppDatabase(parent.getContext());
            final RecyclerPositionDao ReDao = db.recyclerPositionDao();
            final RecyclerPosition recyclerPosition = new RecyclerPosition(0);

        }

        @Override
        public void onClick(View v) {
            adapterposition = getAdapterPosition();

        }



        public void bind(ProductForAssem productForAssem) {

            ProAssemText.setText(productForAssem.getProducto());
            ProdQtyText.setText(productForAssem.getCantidad());
            ProPrzText.setText(productForAssem.getCosto());

            id = productForAssem.getProductoId();

        }



    }


    private List<ProductForAssem> productForAssems;

    public AssemblieDetailsAdapter(Context context, List<ProductForAssem> productForAssems) {
        this.context = context;
        this.productForAssems = productForAssems;
    }

    public Integer getIdProduct(int position) {
        ProductForAssem productForAssem = new ProductForAssem("0",0,"0","0");
        productForAssem = productForAssems.get(position);
        return productForAssem.getProductoId();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.assemblie_details_item, viewGroup, false);
        ((Activity)viewGroup.getContext()).registerForContextMenu(view);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.bind(productForAssems.get(i));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Product_details.class);
                intent.putExtra(Product_details.CURRENT_PRODUCT_INDEX_EXTRA, viewHolder.id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productForAssems.size();
    }




}


public class AssemblieDetails extends AppCompatActivity {

    public static final String CURRENT_ASSEMBLIE_INDEX_EXTRA = "COM.EXAMPLE.NEWASSEMBLIE.CURRENT_ASSEMBLIE";
    public static final int ASSEMBLIE_REQUEST_CODE = 1;

    private int CurrentAssemblie;


    private TextView EnsambleTxt;
    private RecyclerView recyclerView;
    private  TextView CostoEnsambleTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assemblie_details);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);

        EnsambleTxt = findViewById(R.id.EnsambleText);
        CostoEnsambleTxt = findViewById(R.id.CostoEnsambleText);


        Intent intent = getIntent();
        CurrentAssemblie = intent.getIntExtra(CURRENT_ASSEMBLIE_INDEX_EXTRA,0);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());;
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        AssembliesDao AsDao = db.AssembliesDao();

        EnsambleTxt.setText("Nombre del Ensamble: " + AsDao.getAssembliesDesById(CurrentAssemblie));

        List<Integer> productos = new ArrayList<>();
        List<Integer> qtyPr = new ArrayList<>();
        double Precio = 0;
        double temp;
        productos = AsprDao.getProductsById(CurrentAssemblie);
        qtyPr = AsprDao.getProductsQtyById(CurrentAssemblie);
        for (int j = 0; j < productos.size(); j++){
            temp = PrDao.getProductsPriceById(productos.get(j))/100;
            temp = temp * qtyPr.get(j);
            Precio = Precio + temp;
        }

        CostoEnsambleTxt.setText("Costo del ensamble: $" + obtieneDosDecimales(Precio));
        Precio = 0;
        qtyPr.clear();
        productos.clear();

        List<ProductForAssem> productForAssems = new ArrayList<>();
        List<Integer> productos2 = new ArrayList<>();
        List<Integer> qtyPr2 = new ArrayList<>();

        productos2 = AsprDao.getProductsById(CurrentAssemblie);
        qtyPr2 = AsprDao.getProductsQtyById(CurrentAssemblie);
        for (int i = 0; i < productos2.size(); i++){
            double precio =PrDao.getProductsPriceById(productos2.get(i))/100;
            ProductForAssem productForAssem = new ProductForAssem("0",0,"0","0");
            productForAssem.setCantidad(String.valueOf(qtyPr2.get(i)));
            productForAssem.setCosto(String.valueOf(obtieneDosDecimales(precio)));
            productForAssem.setProducto(PrDao.getProductsDesById(productos2.get(i)));
            productForAssem.setProductoId(productos2.get(i));
            productForAssems.add(productForAssem);
        }

        recyclerView = findViewById(R.id.recycler_view4);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new AssemblieDetailsAdapter(this,productForAssems));

    }

    @TargetApi(24)
    private String obtieneDosDecimales(double valor){
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        return format.format(valor);
    }
}
