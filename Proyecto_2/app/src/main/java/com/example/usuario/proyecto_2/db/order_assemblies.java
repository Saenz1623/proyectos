package com.example.usuario.proyecto_2.db;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "order_assemblies")
public class Order_assemblies {

    @PrimaryKey(autoGenerate = true)
    private int oras_id;


    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "assembly_id")
    private int assembly_id;

    @ColumnInfo(name = "qty")
    private int qty;

    public int getOras_id() {
        return oras_id;
    }

    public void setOras_id(int oras_id) {
        this.oras_id = oras_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAssembly_id() {
        return assembly_id;
    }

    public void setAssembly_id(int assembly_id) {
        this.assembly_id = assembly_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Order_assemblies(int oras_id, int id, int assembly_id, int qty) {
        this.oras_id = oras_id;
        this.id = id;
        this.assembly_id = assembly_id;
        this.qty = qty;
    }
}
