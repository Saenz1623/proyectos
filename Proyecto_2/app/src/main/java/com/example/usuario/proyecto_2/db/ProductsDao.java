package com.example.usuario.proyecto_2.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.widget.Space;

import java.util.List;

@Dao
public interface ProductsDao {

    @Insert
    public void InsertProducts(Products product);

    @Update
    public void UpdateProducts(Products product);

    @Query("SELECT * FROM products ORDER BY description ASC")
    public List<Products> getAllProducts();

    @Query("SELECT * FROM products WHERE id = :id")
    public List<Products> getProductsById(int id);

    @Query("SELECT price FROM products WHERE id = :id")
    public double getProductsPriceById(int id);

    @Query("SELECT description FROM products WHERE id = :id")
    public String getProductsDesById(int id);

    @Query("SELECT * FROM products WHERE id = :id")
    public Products getOneProductsById(int id);

    @Query("SELECT * FROM products WHERE category_id = :id ORDER BY description ASC")
    public List<Products> getProductsByCategory(int id);

    @Query("SELECT * FROM products WHERE description LIKE :des ORDER BY description ASC ")
    public List<Products> getProductsByDescription(String des);

    @Query("SELECT * FROM products WHERE category_id = :id AND description LIKE :des ORDER BY description ASC ")
    public List<Products> getProductsByDescriptionAndCategory(String des, int id);

}
