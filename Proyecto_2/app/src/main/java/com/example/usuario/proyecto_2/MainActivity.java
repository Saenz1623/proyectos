package com.example.usuario.proyecto_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    private ImageButton OrderButton;
    private ImageButton ProductButton;
    private ImageButton AssemblieButton;
    private ImageButton CustomersButton;
    private ImageButton SimularButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        OrderButton = findViewById(R.id.OrdersButton);
        ProductButton = findViewById(R.id.ProductsButton);
        AssemblieButton = findViewById(R.id.AssembliesButton);
        CustomersButton = findViewById(R.id.UnoButton);
        SimularButton = findViewById(R.id.cuatroButton);

        OrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OrdersActivity.class);
                startActivity(intent);
            }
        });

        ProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                startActivity(intent);
            }
        });

        AssemblieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AssemblieActivity.class);
                startActivity(intent);
            }
        });

        CustomersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CustomersActivity.class);
                startActivity(intent);
            }
        });

        SimularButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SimularActivity.class);
                startActivity(intent);
            }
        });


    }
}
