package com.example.usuario.proyecto_2;

import android.annotation.TargetApi;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.AssembliesDao;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.Product_CategoriesDao;
import com.example.usuario.proyecto_2.db.Products;
import com.example.usuario.proyecto_2.db.ProductsDao;

public class Product_details extends AppCompatActivity {

    public static final String CURRENT_PRODUCT_INDEX_EXTRA = "COM.EXAMPLE.ASSEMBLIEDETAILS.CURRENT_ASSEMBLIE";
    public static final int PRODUCT_REQUEST_CODE = 1;

    private int CurrentProduct;

    private TextView ProductoDetailsText;
    private TextView ProductoCatTetailsText;
    private TextView ProductoPriceDetailsText;
    private TextView ProductoQtyDetailsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar5);
        setSupportActionBar(toolbar);

        ProductoDetailsText = findViewById(R.id.ProductoDetailsText);
        ProductoCatTetailsText = findViewById(R.id.ProductoCategoriasDetailsText);
        ProductoPriceDetailsText = findViewById(R.id.ProductoPrecioDetailsText);
        ProductoQtyDetailsText = findViewById(R.id.ProductoQtyDetailsText);

        Intent intent = getIntent();
        CurrentProduct = intent.getIntExtra(CURRENT_PRODUCT_INDEX_EXTRA,0);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        ProductsDao PrDao = db.productsDao();
        Product_CategoriesDao PcDao = db.product_CategoriesDao();

        Products products = PrDao.getOneProductsById(CurrentProduct);

        String category = PcDao.getProCategoriesDesById(products.getCategory_id());

        double Precio = products.getPrice()/100;

        ProductoDetailsText.setText("Producto: " + products.getDescription());
        ProductoQtyDetailsText.setText("Cantidad en el almacen: " + products.getQty());
        ProductoPriceDetailsText.setText("Precio del producto: " + String.valueOf(obtieneDosDecimales(Precio)));
        ProductoCatTetailsText.setText("Categoria del producto: " + category);

    }

    @TargetApi(24)
    private String obtieneDosDecimales(double valor){
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        return format.format(valor);
    }
}
