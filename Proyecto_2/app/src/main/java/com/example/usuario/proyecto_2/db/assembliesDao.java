package com.example.usuario.proyecto_2.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface AssembliesDao {

    @Insert
    public void InsertAssemblies(Assemblies assemblies);

    @Update
    public void UpdateAssemblies(Assemblies assemblies);

    @Query("SELECT * FROM assemblies ORDER BY id")
    public List<Assemblies> getAllassemblies();

    @Query("SELECT * FROM assemblies WHERE id = :id")
    public List<Assemblies> getAssembliesById(int id);

    @Query("SELECT description FROM assemblies WHERE id = :id")
    public String getAssembliesDesById(int id);

    @Query("SELECT * FROM assemblies WHERE description LIKE :description ORDER BY id")
    public List<Assemblies> getassembliesByDescription(String description);

}
