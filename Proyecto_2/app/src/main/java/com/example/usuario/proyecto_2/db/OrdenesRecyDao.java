package com.example.usuario.proyecto_2.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OrdenesRecyDao {

    @Insert
    public void InsertOrden(OrdenesRecy ordenesRecies);

    @Query("SELECT * FROM OrdenesRecy ORDER BY id")
    public List<OrdenesRecy> getAll();

    @Query("DELETE FROM OrdenesRecy")
    public void nukeTable();
}
