package com.example.usuario.proyecto_2;

public class ProductForAssem {
    private String Producto;
    private int ProductoId;
    private String Cantidad;
    private String Costo;

    public String getProducto() {
        return Producto;
    }

    public void setProducto(String producto) {
        Producto = producto;
    }

    public int getProductoId() {
        return ProductoId;
    }

    public void setProductoId(int productoId) {
        ProductoId = productoId;
    }

    public String getCantidad() {
        return Cantidad;
    }

    public void setCantidad(String cantidad) {
        Cantidad = cantidad;
    }

    public String getCosto() {
        return Costo;
    }

    public void setCosto(String costo) {
        Costo = costo;
    }

    public ProductForAssem(String producto, int productoId, String cantidad, String costo) {
        Producto = producto;
        ProductoId = productoId;
        Cantidad = cantidad;
        Costo = costo;
    }
}
