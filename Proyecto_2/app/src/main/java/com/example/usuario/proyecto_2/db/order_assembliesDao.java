package com.example.usuario.proyecto_2.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Order_assembliesDao {

    @Insert
    public void InsertOrderAssem(Order_assemblies order_assemblies);

    @Update
    public void UpdateOrderAssem(Order_assemblies order_assemblies);

    @Delete
    public void DeleteOrderAssem(Order_assemblies order_assemblies);

    @Query("SELECT * FROM order_assemblies ORDER BY id")
    public List<Order_assemblies> getAllOrAssem();

    @Query("SELECT * FROM order_assemblies WHERE id = :id")
    public List<Order_assemblies> getOrAssemById(int id);

    @Query("SELECT SUM(qty) FROM order_assemblies WHERE id = :id")
    public int getNumAssemById(int id);

    @Query("SELECT assembly_id FROM order_assemblies WHERE id = :id")
    public List<Integer> getAssemById(int id);

    @Query("SELECT qty FROM order_assemblies WHERE id = :id")
    public List<Integer> getAssemQtyById(int id);

    @Query("SELECT qty FROM order_assemblies WHERE assembly_id = :id AND id = :orid")
    public Integer getOneAssemQtyById(int id, int orid);

    @Query("SELECT oras_id FROM order_assemblies WHERE id = :id")
    public List<Integer> getRowIdAssemById(int id);

    @Query("SELECT * FROM order_assemblies WHERE oras_id = :id")
    public Order_assemblies getAssemByRowId(int id);

}
