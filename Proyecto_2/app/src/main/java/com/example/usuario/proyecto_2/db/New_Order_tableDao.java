package com.example.usuario.proyecto_2.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface New_Order_tableDao {

    @Insert
    public void InsertOrden(New_Order_Table new_order_table);

    @Query("DELETE FROM New_Order_Table")
    public void nukeTable();

    @Query("SELECT * FROM New_Order_Table ORDER BY newor_id")
    public List<New_Order_Table> getAll();

    @Query("SELECT * FROM New_Order_Table WHERE assembly_id = :assembly_id")
    public New_Order_Table getOrderByAssemid(int assembly_id);

    @Update
    public void UpdateNewOrder(New_Order_Table new_order_table);

    @Delete
    public void DeleteOrder(New_Order_Table new_order_table);


}
