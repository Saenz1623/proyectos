package com.example.usuario.proyecto_2;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.Dialog;


public class DetailsCustomer extends DialogFragment {

    private String message = "Usuario no seleccionado";

    public DetailsCustomer setMessage(String customMessage){
        message = customMessage;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Detalles").setMessage(message);
        return builder.create();

    }
}