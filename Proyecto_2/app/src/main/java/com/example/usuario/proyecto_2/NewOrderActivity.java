package com.example.usuario.proyecto_2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.proyecto_2.db.AppDatabase;
import com.example.usuario.proyecto_2.db.Assembly_productsDao;
import com.example.usuario.proyecto_2.db.Customers;
import com.example.usuario.proyecto_2.db.CustomersDao;
import com.example.usuario.proyecto_2.db.New_Order_Table;
import com.example.usuario.proyecto_2.db.New_Order_tableDao;
import com.example.usuario.proyecto_2.db.OrdenesRecy;
import com.example.usuario.proyecto_2.db.OrdenesRecyDao;
import com.example.usuario.proyecto_2.db.Order_assemblies;
import com.example.usuario.proyecto_2.db.Order_assembliesDao;
import com.example.usuario.proyecto_2.db.Order_statusDao;
import com.example.usuario.proyecto_2.db.Orders;
import com.example.usuario.proyecto_2.db.OrdersDao;
import com.example.usuario.proyecto_2.db.ProductsDao;
import com.example.usuario.proyecto_2.db.RecyclerPosition;
import com.example.usuario.proyecto_2.db.RecyclerPositionDao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

class NewOrderActivityAdapter extends RecyclerView.Adapter<NewOrderActivityAdapter.ViewHolder>{

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView AssemText;
        private TextView QtyText;
        private TextView PrzText;
        private TextView txtPoints;

        private Button btnPlus;
        private Button btnMinus;


        private New_Order_Table new_order_table;

        public ViewHolder (@NonNull final View itemView){
            super(itemView);

            AssemText = itemView.findViewById(R.id.OrAssemText);
            QtyText = itemView.findViewById(R.id.ProQtyText);
            PrzText = itemView.findViewById(R.id.AssemPrzText);
            txtPoints = itemView.findViewById(R.id.points_text);

            btnPlus = itemView.findViewById(R.id.plus_button);
            btnMinus = itemView.findViewById(R.id.minus_button);


            final View parent = itemView;

            AppDatabase db = AppDatabase.getAppDatabase(parent.getContext());
            final RecyclerPositionDao ReDao = db.recyclerPositionDao();
            final RecyclerPosition recyclerPosition = new RecyclerPosition(0);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // get position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if(pos != RecyclerView.NO_POSITION){
                        ReDao.nukeTable();
                        recyclerPosition.setPosition(pos);
                        ReDao.InsertPosition(recyclerPosition);
                    }
                    return false;
                }
            });

            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new_order_table.setQty(new_order_table.getQty() + 1);
                    txtPoints.setText(String.valueOf(new_order_table.getQty()));
                    AppDatabase.getAppDatabase(parent.getContext()).new_order_tableDao().UpdateNewOrder(new_order_table);
                }
            });

            btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int number = new_order_table.getQty();
                    if (number == 1){
                        Toast t=Toast.makeText(parent.getContext(),"Mantenga presionado la orden si desea eliminarla", Toast.LENGTH_SHORT);
                        t.show();
                    }else {
                        new_order_table.setQty(new_order_table.getQty() - 1);
                        txtPoints.setText(String.valueOf(new_order_table.getQty()));
                        AppDatabase.getAppDatabase(parent.getContext()).new_order_tableDao().UpdateNewOrder(new_order_table);
                    }
                }
            });

        }

        @Override
        public void onClick(View v) {
            int adapterposition;
            adapterposition = getAdapterPosition();

        }



        public void bind(New_Order_Table new_order_table) {

            this.new_order_table = new_order_table;

            AssemText.setText(new_order_table.getAssembly());;
            QtyText.setText(new_order_table.getQtyProducs());
            PrzText.setText(new_order_table.getPrice());
            txtPoints.setText(String.valueOf(new_order_table.getQty()));
        }



    }

    private List<New_Order_Table> new_order_tables;

    public NewOrderActivityAdapter(List<New_Order_Table> new_order_tables) {
        this.new_order_tables = new_order_tables;
    }

    public New_Order_Table getItem(int position) {
        return new_order_tables.get(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.new_assem_item, viewGroup, false);
        ((Activity)viewGroup.getContext()).registerForContextMenu(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(new_order_tables.get(i));
    }

    @Override
    public int getItemCount() {
        return new_order_tables.size();
    }
}

public class NewOrderActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private Spinner ClientSpinner;

    private ImageButton NewAssem;

    private Button SaveAssem;

    public static final int NEWASSEMBLIE_REQUEST_CODE = 1;

    Calendar cr;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        ClientSpinner = findViewById(R.id.ClientSpinner2);
        NewAssem = findViewById(R.id.AddAssemButton);

        SaveAssem = findViewById(R.id.SaveAssemButton);




        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        final OrdersDao OrDao = db.OrdersDao();
        final CustomersDao CuDao = db.customersDao();
        final Order_assembliesDao OrAsDao = db.Order_assembliesDao();
        Assembly_productsDao AsprDao = db.Assembly_productsDao();
        ProductsDao PrDao = db.productsDao();
        final New_Order_tableDao NewOrDao = db.new_order_tableDao();

        List<Customers> Clientes = CuDao.getAllCustomers();
        ArrayAdapter<String> Clientadapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);

        for (int i = 0; i < Clientes.size(); i++){
            Clientadapter.add(Clientes.get(i).getFirst_name() + " " + Clientes.get(i).getLast_name());
        }

        ClientSpinner.setAdapter(Clientadapter);


        List<New_Order_Table> new_order_tables = NewOrDao.getAll();

        recyclerView = findViewById(R.id.recycler_view2);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new NewOrderActivityAdapter(new_order_tables));

        NewAssem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewOrderActivity.this, NewAssemblie.class);
                startActivityForResult(intent,NewAssemblie.NEWASSEMBLIE_REQUEST_CODE);

            }
        });

        SaveAssem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cr = Calendar.getInstance();

                final int day = cr.get(Calendar.DAY_OF_MONTH);
                final int month = cr.get(Calendar.MONTH);
                final int year = cr.get(Calendar.YEAR);


                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(NewOrderActivity.this);
                dialogo1.setTitle("Importante");
                dialogo1.setMessage("Despues de guardar los datos, esta ventana se cerrará y los datos serán enviados a la base de datos");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Orders orders = new Orders(0,0,0,"0","0");
                        int Customer = ClientSpinner.getSelectedItemPosition();
                        List<Customers> customers = CuDao.getAllCustomers();
                        int Customertemp = Customer;
                        while (customers.get(Customertemp).getId() != Customer){
                            Customer ++;
                        }
                        orders.setCustomer_id(Customer);
                        orders.setDate(String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year));
                        orders.setStatus_id(0);
                        orders.setChange_log("");

                        List<New_Order_Table> new_order_tables1 = new ArrayList<>();
                        new_order_tables1 = NewOrDao.getAll();

                        if(new_order_tables1 == null || new_order_tables1.size() == 0){
                            Toast t=Toast.makeText(NewOrderActivity.this,"Por favor, añada ensambles a la orden", Toast.LENGTH_SHORT);
                            t.show();
                        }else {

                            orders.setId(new_order_tables1.get(0).getOr_id());

                            OrDao.InsertOrder(orders);

                            for (int i = 0; i < new_order_tables1.size(); i++) {
                                Order_assemblies order_assemblies = new Order_assemblies(0, 0, 0, 0);
                                order_assemblies.setAssembly_id(new_order_tables1.get(i).getAssembly_id());
                                order_assemblies.setId(new_order_tables1.get(i).getOr_id());
                                order_assemblies.setQty(new_order_tables1.get(i).getQty());
                                OrAsDao.InsertOrderAssem(order_assemblies);
                            }
                            Toast t = Toast.makeText(NewOrderActivity.this, "Orden añadida con exito", Toast.LENGTH_SHORT);
                            t.show();
                            setResult(RESULT_OK);
                            NewOrDao.nukeTable();
                            finish();
                        }

                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Toast t=Toast.makeText(NewOrderActivity.this,"Se ha cancelado la operación", Toast.LENGTH_SHORT);
                        t.show();
                    }
                });
                dialogo1.show();

            }
        });


    }

    @Override
    public void onBackPressed() {
        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        final New_Order_tableDao NewOrDao = db.new_order_tableDao();

        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("Si sale de esta ventana sus datos no seran guardados");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                NewOrDao.nukeTable();
                NewOrderActivity.super.onBackPressed();

            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                Toast t=Toast.makeText(getApplicationContext(),"Se ha cancelado la operación", Toast.LENGTH_SHORT);
                t.show();
            }
        });
        dialogo1.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        final New_Order_tableDao NewOrDao = db.new_order_tableDao();
        List<New_Order_Table> new_order_tables = NewOrDao.getAll();

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case NewAssemblie.NEWASSEMBLIE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    recyclerView = findViewById(R.id.recycler_view2);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerView.setAdapter(new NewOrderActivityAdapter(new_order_tables));
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

            default:
                // other activities...
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.delete_item, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final RecyclerView recyclerView;

        recyclerView = findViewById(R.id.recycler_view2);

        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
        RecyclerPositionDao ReDao = db.recyclerPositionDao();
        final New_Order_tableDao NeDao = db.new_order_tableDao();
        final List<New_Order_Table> new_order_tables = NeDao.getAll();

        final New_Order_Table new_order_table;

        NewOrderActivityAdapter newOrderActivityAdapter = (NewOrderActivityAdapter) recyclerView.getAdapter();
        int position = ReDao.getposition();

        switch (item.getItemId()) {

            case R.id.DeleteAssem_item:
                new_order_table = newOrderActivityAdapter.getItem(position);
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                dialogo1.setTitle("Importante");
                dialogo1.setMessage("¿Desea eliminar el ensamble?" + " " + new_order_table.getAssembly());
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        NeDao.DeleteOrder(new_order_table);
                        Toast t=Toast.makeText(getApplicationContext(),"Se ha eliminado el ensamble", Toast.LENGTH_SHORT);
                        t.show();
                        List<New_Order_Table> new_order_tables = NeDao.getAll();
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        recyclerView.setAdapter(new NewOrderActivityAdapter(new_order_tables));
                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Toast t=Toast.makeText(getApplicationContext(),"Se ha cancelado la operación", Toast.LENGTH_SHORT);
                        t.show();
                    }
                });
                dialogo1.show();

                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }
}
