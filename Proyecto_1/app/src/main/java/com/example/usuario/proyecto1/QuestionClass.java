package com.example.usuario.proyecto1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// This class can simulate an access to the database while using the same instance
public class QuestionClass {

    private List<Question> Tecnologia;
    private List<Question> Matematicas;
    private List<Question> Deportes;
    private List<Question> Fisica;
    private List<Question> Historia;
    private List<Question> Cultura_general;
    private List<Question> Questionall;

    public static int OptionLevel = 1;


    public List<Question> getAllTecnologia() {
        if (Tecnologia == null) {
            Tecnologia = new ArrayList<>();
            Tecnologia.add(new Question(0, 1, "¿El mouse es parte del hardware en una computadora?", true, 0));
            Tecnologia.add(new Question(1, 1, "¿El sistema operativo forma parte del software en una caomputadora?", true, 0));
            Tecnologia.add(new Question(2, 1, "La conexion bluetooth te permite conectarte a internet", false, 0));
            Tecnologia.add(new Question(3, 1, "¿La mayoria de los teclados usan la configuracion qwerty?", true, 0));
            Tecnologia.add(new Question(4, 1, "¿Whatsapp te permite enviar documentos?", true, 0));
            Tecnologia.add(new Question(5, 2, "¿Es HTML un lenguaje de programacion web?", true, 0));
            Tecnologia.add(new Question(6, 2, "¿Las computadoras usan el lenguaje de unos y ceros para realizar sus operaciones?", true, 0));
            Tecnologia.add(new Question(7, 2, "¿Linux es un sistema operativo de software libre?", true, 0));
            Tecnologia.add(new Question(8, 2, "¿Los aviones son un invento del siglo XX?", true, 0));
            Tecnologia.add(new Question(9, 2, "¿Los telefonos SAMSUNG han sido los mas populares desde simpre?", false, 0));
            Tecnologia.add(new Question(10, 3, "¿C# es un lenguaje de programacion enfocado al uso de clases?", true, 0));
            Tecnologia.add(new Question(11, 3, "¿Java soporta sobrecarga de operadores?", false, 0));
            Tecnologia.add(new Question(12, 3, "¿Existen avione sque pueden volar a mas de 2000 km/h?", true, 0));
            Tecnologia.add(new Question(13, 3, "¿El litio se utiliza para la fabricacion de baterias?", true, 0));
            Tecnologia.add(new Question(14, 3, "La plata se utiliza en la fabricacion de celulares por su alta conductividad", false, 0));
        }

        return Tecnologia;
    }

    public List<Question> getAllMatematicas() {
        if (Matematicas == null) {
            Matematicas = new ArrayList<>();
            Matematicas.add(new Question(15, 1, "26 más 30 es igual a 56", true, 0));
            Matematicas.add(new Question(16, 1, "3 por 9 es igual a 29", false, 0));
            Matematicas.add(new Question(17, 1, "la suma de los angulos internos de un cuadrado es 180", false, 0));
            Matematicas.add(new Question(18, 1, "¿La raíz de 8 es igual al doble la raíz de 2?", true, 0));
            Matematicas.add(new Question(19, 1, "¿En las operaciones matematicas, siempre se realizan primero las multiplicaciones y luego las sumas?", true, 0));
            Matematicas.add(new Question(20, 2, "¿El cuadrado de un binomio es igual al cuadrado del primer termino, mas el doble del primero por el segundo mas el cuadrado del tercero?", true, 0));
            Matematicas.add(new Question(21, 2, "¿El resultado de un binomio conjugado, es igual al cuadrado del primero mas el cuadrado del segundo?", false, 0));
            Matematicas.add(new Question(22, 2, "¿La trigonometria es el area de la geometria que estudia las figuras de 5 o mas lados?", false, 0));
            Matematicas.add(new Question(23, 2, "¿El coseno en un triangulo rectangulo, es el cociente del cateto opuesto del angulo entre la hipotenusa?", true, 0));
            Matematicas.add(new Question(24, 2, "¿El seno en un triangulo rectangulo, es el cociente del cateto opuesto del angulo entre el cateto adyacente?", false, 0));
            Matematicas.add(new Question(25, 3, "¿Un numero compuesto, es aquel que esta conformado por numeros imaginarios y naturales?", true, 0));
            Matematicas.add(new Question(26, 3, "¿Las funciones valor absoluto no son simetricas respect al eje y?", false, 0));
            Matematicas.add(new Question(27, 3, "¿Función trascendente es una función que satisface una ecuación polinomial?", false, 0));
            Matematicas.add(new Question(28, 3, "¿La coordenada (-3,-2) esta en el segundo cuadrante?", false, 0));
            Matematicas.add(new Question(29, 3, "¿2+2(3+4)=16?", true, 0));
        }

        return Matematicas;
    }

    public List<Question> getAllDeportes() {
        if (Deportes == null) {
            Deportes = new ArrayList<>();
            Deportes.add(new Question(30, 1, "¿Julio Cesar Chaves es uno de los mejores boxeadores mexicanos de la historia?", true, 0));
            Deportes.add(new Question(31, 1, "¿Un Home Rome es el mejor golpe que puedes dar en el Beisbol?", true, 0));
            Deportes.add(new Question(32, 1, "Un equipo de beisbol se conforma por 9 jugadores minimo", false, 0));
            Deportes.add(new Question(33, 1, "¿Jorge Campos es el portero mexicano mas reconocidos internacionalmente?", true, 0));
            Deportes.add(new Question(34, 1, "¿En un partido de futbol normalmente hay tres arbitros?", true, 0));
            Deportes.add(new Question(35, 2, "¿Una pelea de box se puede ganar por nock out(incluye el tecnico), o por la tarjeta de los referis?", true, 0));
            Deportes.add(new Question(36, 2, "¿En el boxeo pueden pelear boxeadores de diferentes categorías?", true, 0));
            Deportes.add(new Question(37, 2, "¿En un partido de beisbol hay tres arbitros?", false, 0));
            Deportes.add(new Question(38, 2, "¿El mariscal de campo manda generalmente los pases en el fútbol americano?", true, 0));
            Deportes.add(new Question(39, 2, "¿Diego Armando Maradona anotó con la mano un gol, en el Mundial de México 68?", false, 0));
            Deportes.add(new Question(40, 3, "¿El Beisbol tiene su origen en Estados Unidos?", true, 0));
            Deportes.add(new Question(41, 3, "¿Transcurrieron 20 años entre el primero y el segundo mundiales mexicanos de futbol?", false, 0));
            Deportes.add(new Question(42, 3, "¿Entre Brasil, Alemania e Italia suman 12 mundiales?", false, 0));
            Deportes.add(new Question(43, 3, "¿Francia eliminó a Italia del Mundial de Futbol México 86?", true, 0));
            Deportes.add(new Question(44, 3, " México anotó 5 goles en el Campeonato Mundial de Futbol México 1986", false, 0));
        }

        return Deportes;
    }

    public List<Question> getAllFisica() {
        if (Fisica == null) {
            Fisica = new ArrayList<>();
            Fisica.add(new Question(45, 1, "¿La masa se define como la cantidad de materia de un material?", true, 0));
            Fisica.add(new Question(46, 1, "¿Existe la fuerza centrifuga?", false, 0));
            Fisica.add(new Question(47, 1, "¡¿La energia es otra definicion de fuerza?", false, 0));
            Fisica.add(new Question(48, 1, "¿La masa asi como la energia se conservan tanto en una reaccion fisica como quimica?", true, 0));
            Fisica.add(new Question(49, 1, "¿La aceleracion de la gravedad es de 9.81 m/s?", true, 0));
            Fisica.add(new Question(50, 2, "Un año luz es una unidad de tiempo", false, 0));
            Fisica.add(new Question(51, 2, "¿Segun la teoria de la relatividad, entre mas rapido viajes el tiempo transcurre mas lento?", true, 0));
            Fisica.add(new Question(52, 2, "¿La energia cinetica es aqueya que posee un cuerpo en movimiento?", true, 0));
            Fisica.add(new Question(53, 2, "¿Un trabajo se define como la cantidad de energia por unidad de distancia?", true, 0));
            Fisica.add(new Question(54, 2, "¿Un atomo es la particula mas pequeña en la que se puede dividir un elemento?", true, 0));
            Fisica.add(new Question(55, 3, "¿La velocidad del sonido es constante?", false, 0));
            Fisica.add(new Question(56, 3, "¿Segun Albert Einsten la materia deforma el especio?", true, 0));
            Fisica.add(new Question(57, 3, "¿Las reacciones que liberan energia se denominan endotermicas?", false, 0));
            Fisica.add(new Question(58, 3, "¿La energia fluye de un estado de mayor energia a uno de menor?", true, 0));
            Fisica.add(new Question(59, 3, "La fisica cuantica fue explicada por medio de una caja y un gato", true, 0));
        }

        return Fisica;
    }

    public List<Question> getAllHistoria() {
        if (Historia == null) {
            Historia = new ArrayList<>();
            Historia.add(new Question(60, 1, "México despues de su independencia se estableció como republica", true, 0));
            Historia.add(new Question(61, 1, "Los primeros pobladores que se desplazaban en busca de alimento se llamaban nomadas", true, 0));
            Historia.add(new Question(62, 1, "Que primer metal usado por el hombre fue el hierro", false, 0));
            Historia.add(new Question(63, 1, "¿Italia fue la sede del Imperio Romano?", true, 0));
            Historia.add(new Question(64, 1, "¿En 1500 Cristobla Colon Descubre America?", false, 0));
            Historia.add(new Question(65, 2, "¿Mesopotamia se considera la cuan de la civilizacion?", true, 0));
            Historia.add(new Question(66, 2, "¿El Capitalismo tiene su origen en EEUU?", true, 0));
            Historia.add(new Question(67, 2, "¿La Constitucion de Cadiz se proclamo en 1911?", false, 0));
            Historia.add(new Question(68, 2, "La primer levantamiento armado en contra del propio gobierno de un pais tuvo lugar en Francia", true, 0));
            Historia.add(new Question(69, 2, "¿La revolucion Mexicano tuvo sus comienzos en 1910?", true, 0));
            Historia.add(new Question(70, 3, "¿El teatro nació en la ciudad griega de Atenas en el año de 536 a.C?", true, 0));
            Historia.add(new Question(71, 3, "Alejandro Magno fue conocido como el rey de Macedonia y fue discípulo de Aristóteles", true, 0));
            Historia.add(new Question(72, 3, "¿En el neolitico se domesticaron algunos animales y se comenzó el cultivo de plantas?", true, 0));
            Historia.add(new Question(73, 3, "Juegos olímpicos son originarios del imperio de Roma", false, 0));
            Historia.add(new Question(74, 3, "Portugal descubrió las Islas Canarias y se quedó con ellas", false, 0));
        }

        return Historia;
    }

    public List<Question> getAllCultura_general() {
        if (Cultura_general == null) {
            Cultura_general = new ArrayList<>();
            Cultura_general.add(new Question(45, 1, "¿El nilo es el río más largo del mundo?", true, 0));
            Cultura_general.add(new Question(46, 1, "El Atlantico es océano más grande", false, 0));
            Cultura_general.add(new Question(47, 1, "Jupiter es quinto planeta en el sistema solar", true, 0));
            Cultura_general.add(new Question(48, 1, "¿Una celula es mas grande que un atomo?", true, 0));
            Cultura_general.add(new Question(49, 1, "¿Un Oviparo es una animal que nace a partir de huevos?", true, 0));
            Cultura_general.add(new Question(50, 2, "¿El maiz es de origen mesoamericano?", true, 0));
            Cultura_general.add(new Question(51, 2, "¿Miguel Catedras es el autor de Don Quijote de la Mancha?", false, 0));
            Cultura_general.add(new Question(52, 2, "¿China es el pais con mas habitantes?", true, 0));
            Cultura_general.add(new Question(53, 2, "Alexander Fleming fue quien descubrió la penicilina", true, 0));
            Cultura_general.add(new Question(54, 2, "¿Mexico es una republica Centralista?", false, 0));
            Cultura_general.add(new Question(55, 3, "¿La energia contenida en el nucleo de los atomos se denomidan energia nuclear?", true, 0));
            Cultura_general.add(new Question(56, 3, "¿El murcielago es un mamifero?", true, 0));
            Cultura_general.add(new Question(57, 3, "¿Platon decía la frase Yo solo se , que no se nada", true, 0));
            Cultura_general.add(new Question(58, 3, "Viva la France es el himno nacional de Francia", false, 0));
            Cultura_general.add(new Question(59, 3, "El metal mas caro del mundo es el oro", false, 0));
        }

        return Cultura_general;

    }

    public List<Question> getAllQuestionall() {
        if(Questionall == null){
            Questionall = new ArrayList<>();
            Questionall.clear();

            int [] r1 =new int[OptionActivity.NoPreg];
            int [] r2 =new int[OptionActivity.NoPreg];
            int [] r3 =new int[OptionActivity.NoPreg];
            int [] r4 =new int[OptionActivity.NoPreg];
            int [] r5 =new int[OptionActivity.NoPreg];
            int [] r6 =new int[OptionActivity.NoPreg];

            r1[0]=0;r2[0]=0;r3[0]=0;r4[0]=0;r5[0]=0;r6[0]=0;
            for(int i=0; i<OptionActivity.NoPreg;i=i+1) {



                if (OptionActivity.C1 == 1 && i<OptionActivity.NoPreg) {
                    Random r = new Random();
                    int i1 = r.nextInt(14 - 0);
                    int repeat=0;

                    for(int j=i; j==0; j--) {
                        if (i1 == r1[i]) {
                            repeat = 1;
                            j = 0;
                        }else{
                            if(j==0){
                                r1[i]=i1;
                            }
                        }

                    }
                    int level = getAllTecnologia().get(i1).getLevel();
                    if (OptionActivity.L1 >= level && repeat==0) {
                        Questionall.add(getAllTecnologia().get(i1));
                        i++;
                    }
                }

                if (OptionActivity.C2 == 1 && i<OptionActivity.NoPreg) {
                    Random r = new Random(); int i1 = r.nextInt(14 - 0);

                    int repeat=0;

                    for(int j=i; j==0; j--) {
                        if (i1 == r2[i]) {
                            repeat = 1;
                            j = 0;
                        }else{
                            if(j==0){
                                r2[i]=i1;
                            }
                        }

                    }

                    int level = getAllMatematicas().get(i1).getLevel();
                    if (OptionActivity.L1 >= level && repeat==0){
                        Questionall.add(getAllMatematicas().get(i1));
                        i++;
                    }
                }
                if (OptionActivity.C3 == 1 && i<OptionActivity.NoPreg) {
                    Random r = new Random(); int i1 = r.nextInt(14 - 0);

                    int repeat=0;

                    for(int j=i; j==0; j--) {
                        if (i1 == r3[i]) {
                            repeat = 1;
                            j = 0;
                        }else{
                            if(j==0){
                                r3[i]=i1;
                            }
                        }

                    }

                    int level = getAllFisica().get(i1).getLevel();
                    if (OptionActivity.L1 >= level && repeat==0){
                        Questionall.add(getAllFisica().get(i1));
                        i++;
                    }
                }
                if (OptionActivity.C4 == 1 && i<OptionActivity.NoPreg) {
                    Random r = new Random(); int i1 = r.nextInt(14 - 0);

                    int repeat=0;

                    for(int j=i; j==0; j--) {
                        if (i1 == r4[i]) {
                            repeat = 1;
                            j = 0;
                        }else{
                            if(j==0){
                                r4[i]=i1;
                            }
                        }

                    }

                    int level = getAllDeportes().get(i1).getLevel();
                    if (OptionActivity.L1 >= level && repeat==0){
                        Questionall.add(getAllDeportes().get(i1));
                        i++;
                    }
                }
                if (OptionActivity.C5 == 1 && i<OptionActivity.NoPreg) {
                    Random r = new Random(); int i1 = r.nextInt(14 - 0);

                    int repeat=0;

                    for(int j=i; j==0; j--) {
                        if (i1 == r5[i]) {
                            repeat = 1;
                            j = 0;
                        }else{
                            if(j==0){
                                r5[i]=i1;
                            }
                        }

                    }

                    int level = getAllHistoria().get(i1).getLevel();
                    if (OptionActivity.L1 >= level && repeat==0){
                        Questionall.add(getAllHistoria().get(i1));
                        i++;
                    }
                }
                if (OptionActivity.C6 == 1 && i<OptionActivity.NoPreg) {
                    Random r = new Random(); int i1 = r.nextInt(14 - 0);

                    int repeat=0;

                    for(int j=i; j==0; j--) {
                        if (i1 == r6[i]) {
                            repeat = 1;
                            j = 0;
                        }else{
                            if(j==0){
                                r6[i]=i1;
                            }
                        }

                    }

                    int level = getAllCultura_general().get(i1).getLevel();
                    if (OptionActivity.L1 >= level && repeat==0){
                        Questionall.add(getAllCultura_general().get(i1));
                        i++;
                    }
                }
                i--;
            }
        }
        return Questionall;
    }
}