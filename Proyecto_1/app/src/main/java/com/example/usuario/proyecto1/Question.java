package com.example.usuario.proyecto1;

class Question {
    private int id;
    private int level;
    private String text;
    private boolean answer;
    private int answered;

    public Question(int id, int level, String text, boolean answer, int answered) {
        this.id = id;
        this.level = level;
        this.text = text;
        this.answer = answer;
        this.answered = answered;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    public int getAnswered() {
        return answered;
    }

    public void setAnswered(int answered) {
        this.answered = answered;
    }

}
