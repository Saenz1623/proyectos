package com.example.usuario.proyecto1;

import android.content.Intent;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button StartButton;
    private Button SettingsButton;


    public static int Preg;
    public static int Cheatb;

    private int Coin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StartButton = findViewById(R.id.StartButton);
        SettingsButton = findViewById(R.id.SettingsButton);

        Coin = 0;



        SettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OptionActivity.class);
                startActivityForResult(intent, OptionActivity.Option_Activity_Coin);
                Coin = OptionActivity.Option_Activity_Coin;
            }
        });


        StartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, QuestionActivity.class);
                if (Coin == 0){
                    Toast.makeText(MainActivity.this, "Por favor, seleccione primero sus opciones", Toast.LENGTH_SHORT).show();
                }
                if (Coin == 1){
                startActivity(intent);} else { Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();}
            }
        });

    }
}
