package com.example.usuario.proyecto1;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ResultActivity extends AppCompatActivity {

    public static int Trampa = 0;
    public static String Nombre = "AAA";
    public static int Puntaje;
    public int puesto = 0;
    public static int Dif = 1;
    private int PuntajeFin = 0;

    public static int NoPreg = 0;

    private int Porc = 0;

    public TextView Primer_Lugar;
    public TextView Segundo_Lugar;
    public TextView Tercer_Lugar;
    public TextView Cuarto_Lugar;
    public TextView Quinto_Lugar;
    public TextView Sexto_Lugar;

    public ImageView Resul;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Primer_Lugar = findViewById(R.id.Primer_Lugar_Text);
        Segundo_Lugar = findViewById(R.id.Segundo_Lugar_Text);
        Tercer_Lugar = findViewById(R.id.Tercer_Lugar_Text);
        Cuarto_Lugar = findViewById(R.id.Cuarto_Lugar_Text);
        Quinto_Lugar = findViewById(R.id.Quinto_Lugar_Text);
        Sexto_Lugar = findViewById(R.id.Sexto_Lugar_Text);
        Resul = findViewById(R.id.Result);


        PuntajeFin = Puntaje*Dif;

        if (Trampa == 1){
            if (PuntajeFin > 30){
                puesto = 4;
            }
            if (PuntajeFin > 20 && PuntajeFin <= 25){
                puesto = 5;
            }
            if (PuntajeFin > 15 && PuntajeFin <= 25){
                puesto = 6;
            }
            Toast.makeText(ResultActivity.this,"Hiciste Trampa!!!",Toast.LENGTH_SHORT).show();
        } if(Trampa == 0) {
            if (PuntajeFin >= 45){
                puesto = 1;
            }
            if (PuntajeFin >= 38 && PuntajeFin < 45){
                puesto = 2;
            }
            if (PuntajeFin > 30 && PuntajeFin < 38){
                puesto = 3;
            }
            if (PuntajeFin > 25 && PuntajeFin <= 30){
                puesto = 4;
            }
            if (PuntajeFin > 20 && PuntajeFin <= 25 ){
                puesto = 5;
            }
            if (PuntajeFin > 15 && PuntajeFin <= 20){
                puesto = 6;
            }
        }

        Porc = (Puntaje*100)/(NoPreg);

        if (savedInstanceState != null){
            puesto = savedInstanceState.getInt("PUESTO_KEY",0);
            Porc = savedInstanceState.getInt("PORC_KEY",0);
        }



        if (Porc <= 33 && Porc >= 0)
        {
            Resul.setImageResource(R.drawable.bad);
        }
        if (Porc <= 66 && Porc > 33)
        {
            Drawable icon = getDrawable(R.drawable.med);
            icon.setBounds(0, 0, 160, 160);
            Resul.setImageDrawable(icon);
        }
        if (Porc <= 100 && Porc > 66)
        {
            Drawable icon = getDrawable(R.drawable.ood);
            icon.setBounds(0, 0, 160, 160);
            Resul.setImageDrawable(icon);
        }



        if (puesto == 1){
            Primer_Lugar.setText("1.- " + Nombre);
            Segundo_Lugar.setText("2.- GOD");
            Tercer_Lugar.setText("3.- EMZ");
            Cuarto_Lugar.setText("4.- EDS");
            Quinto_Lugar.setText("5.- POL");
            Sexto_Lugar.setText("6.- CAE");
        }
        if (puesto == 2){
            Segundo_Lugar.setText("2.- " + Nombre);
            Tercer_Lugar.setText("3.- EMZ");
            Cuarto_Lugar.setText("4.- EDS");
            Quinto_Lugar.setText("5.- POL");
            Sexto_Lugar.setText("6.- CAE");
        }
        if (puesto == 3){
            Tercer_Lugar.setText("3.- " + Nombre);
            Cuarto_Lugar.setText("4.- EDS");
            Quinto_Lugar.setText("5.- POL");
            Sexto_Lugar.setText("6.- CAE");
        }
        if (puesto == 4){
            Cuarto_Lugar.setText("4.-" + Nombre);
            Quinto_Lugar.setText("5.- POL");
            Sexto_Lugar.setText("6.- CAE");
        }
        if (puesto == 5){
            Quinto_Lugar.setText("5.- " + Nombre);
            Sexto_Lugar.setText("6.- CAE");
        }
        if(puesto == 6){
            Sexto_Lugar.setText("6.- " + Nombre);
        }

        Toast.makeText(ResultActivity.this,"¡Su puntaje fue de: ! " + PuntajeFin ,Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("PUESTO_KEY",puesto);
        outState.putInt("PORC_KEY", Porc);
    }
}
