package com.example.usuario.proyecto1;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class QuestionActivity extends AppCompatActivity {

    private Button previousButton;
    private Button nextButton;
    private Button falseButton;
    private Button trueButton;
    private Button cheatButton;
    private TextView questiontext;
    private TextView QuestionRange;
    private TextView CheatRange;

    private int Counter = 0;
    private int QCounter = 1;

    public static int Nopreg;

    public static int CheatEn;
    private int CheatB = 0;

    public static int CheatC;

    public static int Puntaje = 0;

    private String RespCheat;

    private int Answered;

    private int Respondidas;

    public static int Trampa = 0;


    MainActivityModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        model = ViewModelProviders.of(this).get(MainActivityModel.class);
        model.loadGameQuestion();

        previousButton = findViewById(R.id.previous_button);
        nextButton = findViewById(R.id.next_button);
        falseButton = findViewById(R.id.false_button);
        trueButton = findViewById(R.id.true_button);
        questiontext = findViewById(R.id.question_text);
        cheatButton = findViewById(R.id.cheat_button);
        QuestionRange = findViewById(R.id.QuestionRange);
        CheatRange=findViewById(R.id.CheatRange);

        Intent intent = getIntent();
        Respondidas = 0;
        Puntaje = 0;
        Trampa = 0;

        if (CheatEn == 1){
            CheatRange.setEnabled(true); cheatButton.setEnabled(true);
        } else {CheatRange.setEnabled(false); cheatButton.setEnabled(false);}

        if (savedInstanceState != null){
            Counter = savedInstanceState.getInt("COUNTER_KEY",0);
            CheatB = savedInstanceState.getInt("BUTTON_KEY",0);
            Answered = savedInstanceState.getInt("ANSWERED_KEY",0);
            QCounter = savedInstanceState.getInt("QCOUNTER_KEY",1);
            Respondidas = savedInstanceState.getInt("RESPONDIDAS_KEY",0);
            Trampa = savedInstanceState.getInt("TRAMPA_KEY",0);
        }
        QuestionRange.setText(QCounter + "/" + Nopreg);
        CheatRange.setText(Counter + "/" + CheatC);
        questiontext.setText(model.getCurrentQuestion().getText());

        if (CheatB == 1)
        {
            cheatButton.setEnabled(false);
        }
        if(Answered == 1){
            trueButton.setEnabled(false);
            falseButton.setEnabled(false);
        }else {
            trueButton.setEnabled(true);
            falseButton.setEnabled(true);}

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (QCounter < Nopreg){
                QCounter = QCounter + 1;
                questiontext.setText(model.getNextQuestion().getText());
                }else {QCounter = 1; questiontext.setText(model.getNextQuestion().getText());}
                QuestionRange.setText(QCounter + "/" + Nopreg);
                Answered = model.getNextQuestion().getAnswered();
                if(Answered == 1){
                    trueButton.setEnabled(false);
                    falseButton.setEnabled(false);
                    if(Counter == CheatC){cheatButton.setEnabled(false);} else {cheatButton.setEnabled(true);}
                }else {
                    trueButton.setEnabled(true);
                    falseButton.setEnabled(true);
                    if(Counter == CheatC){cheatButton.setEnabled(false);} else {cheatButton.setEnabled(true);}}


            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (QCounter > 1){
                    QCounter = QCounter - 1;
                    questiontext.setText(model.getPreviousQuiestion().getText());
                }else {QCounter = Nopreg; questiontext.setText(model.getPreviousQuiestion().getText());}
                QuestionRange.setText(QCounter + "/" + Nopreg);
                Answered = model.getPreviousQuiestion().getAnswered();
                if(Answered == 1){
                    trueButton.setEnabled(false);
                    falseButton.setEnabled(false);
                    if(Counter == CheatC){cheatButton.setEnabled(false);} else {cheatButton.setEnabled(true);}
                }else {
                    trueButton.setEnabled(true);
                    falseButton.setEnabled(true);
                    if(Counter == CheatC){cheatButton.setEnabled(false);} else {cheatButton.setEnabled(true);}}
            }
        });

        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Question questions = model.getCurrentQuestion();
                if (questions.isAnswer()){
                    Toast.makeText(QuestionActivity.this,"¡CORRECTO!",Toast.LENGTH_SHORT).show();
                    Puntaje = Puntaje +1;
                }else
                {
                    Toast.makeText(QuestionActivity.this,"¡INCORRECTO!",Toast.LENGTH_SHORT).show();
                }
                questions.setAnswered(1);
                trueButton.setEnabled(false);
                falseButton.setEnabled(false);
                cheatButton.setEnabled(false);
                Respondidas = Respondidas + 1;
                if (Respondidas == Nopreg){
                    ResultActivity.Trampa = Trampa;
                    ResultActivity.Puntaje = Puntaje;
                    Intent intent2 = new Intent(QuestionActivity.this, ResultActivity.class);
                    startActivity(intent2);
                }
            }
        });


        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Question questions = model.getCurrentQuestion();
                if (!questions.isAnswer()) {
                    Toast.makeText(QuestionActivity.this, "¡CORRECTO!", Toast.LENGTH_SHORT).show();
                    Puntaje = Puntaje + 1;
                } else {
                    Toast.makeText(QuestionActivity.this, "¡INCORRECTO!", Toast.LENGTH_SHORT).show();
                }

                questions.setAnswered(1);
                trueButton.setEnabled(false);
                falseButton.setEnabled(false);
                cheatButton.setEnabled(false);
                Respondidas = Respondidas + 1;
                if (Respondidas == Nopreg){
                    ResultActivity.Trampa = Trampa;
                    ResultActivity.Puntaje = Puntaje;
                    Intent intent2 = new Intent(QuestionActivity.this, ResultActivity.class);
                    startActivity(intent2);
                }
            }
        });

        cheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Counter = Counter + 1;
                CheatRange.setText(Counter + "/" + CheatC);
                if(Counter == CheatC){cheatButton.setEnabled(false); CheatB= 1;}
                Question questions = model.getCurrentQuestion();
                RespCheat = String.valueOf(questions.isAnswer());
                Toast.makeText(QuestionActivity.this,"La respuesta era : " + RespCheat, Toast.LENGTH_SHORT).show();
                Puntaje = Puntaje + 1;
                trueButton.setEnabled(false);
                falseButton.setEnabled(false);
                cheatButton.setEnabled(false);
                Trampa = 1;
                questions.setAnswered(1);
                Answered = 1;
                Respondidas = Respondidas + 1;
                if (Respondidas == Nopreg){
                    ResultActivity.Trampa = Trampa;
                    ResultActivity.Puntaje = Puntaje;
                    Intent intent2 = new Intent(QuestionActivity.this, ResultActivity.class);
                    startActivity(intent2);
                }
            }
        });

        if (Respondidas == Nopreg){
            Intent intent2 = new Intent(QuestionActivity.this, ResultActivity.class);
            startActivity(intent2);
        }


    }

    private static final int INTERVALO = 2000; //2 segundos para salir
    private long tiempoPrimerClick;

    @Override
    public void onBackPressed(){
        if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()){
            super.onBackPressed();
            return;
        }else {
            Toast.makeText(this, "Vuelve a presionar para salir", Toast.LENGTH_SHORT).show();
        }
        tiempoPrimerClick = System.currentTimeMillis();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("COUNTER_KEY", Counter);
        outState.putInt("BUTTON_KEY", CheatB );
        outState.putInt("ANSWERED_KEY", Answered);
        outState.putInt("QCOUNTER_KEY",QCounter);
        outState.putInt("RESPONDIDAS_KEY", Respondidas);
        outState.putInt("TRAMPA_KEY",Trampa);
    }
}

