package com.example.usuario.proyecto1;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class OptionActivity extends AppCompatActivity {

    private CheckBox AllCheckBox;
    private CheckBox Cat1CheckBox;
    private CheckBox Cat2CheckBox;
    private CheckBox Cat3CheckBox;
    private CheckBox Cat4CheckBox;
    private CheckBox Cat5CheckBox;
    private CheckBox Cat6CheckBox;
    public static int C1=0;
    public static int C2=0;
    public static int C3=0;
    public static int C4=0;
    public static int C5=0;
    public static int C6=0;

    public static int L1=0;

    private Spinner QuestionSpinner;

    private RadioButton HighRadio;
    private RadioButton MediumRadio;
    private RadioButton LowRadio;

    private Switch CheatSwitch;

    private Spinner CheatSpinner;
    private TextView CheatText;


    private int Count;
    private int dif;
    private int PregCount;

    public static int NoPreg ;
    public static int Cheatb = 1;
    public static int CheatC = 1;

    public static final int Option_Activity_Coin = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);

        AllCheckBox = findViewById(R.id.AllCheckbox);
        Cat1CheckBox = findViewById(R.id.Cat1Checkbox);
        Cat2CheckBox = findViewById(R.id.Cat2Checkbox);
        Cat3CheckBox = findViewById(R.id.Cat3Checkbox);
        Cat4CheckBox = findViewById(R.id.Cat4Checkbox);
        Cat5CheckBox = findViewById(R.id.Cat5Checkbox);
        Cat6CheckBox = findViewById(R.id.Cat6Checkbox);
        QuestionSpinner = findViewById(R.id.QuestionSpinner);
        CheatSpinner=findViewById(R.id.CheatSpinner);
        CheatSwitch = findViewById(R.id.CheatSwitch);
        CheatText = findViewById(R.id.CheatText);
        HighRadio = findViewById(R.id.HighRadio);
        MediumRadio = findViewById(R.id.MediumRadio);
        LowRadio = findViewById(R.id.LowRadio);

        ArrayAdapter<Integer> adapter2 = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item);
        adapter2.add(1);
        adapter2.add(2);
        adapter2.add(3);
        CheatSpinner.setAdapter(adapter2);

        if (CheatSwitch.isChecked()){
            CheatSpinner.setEnabled(true);
            CheatText.setEnabled(true);
            Cheatb = 1;
            QuestionActivity.CheatEn = Cheatb;
        }else {CheatSpinner.setEnabled(false); CheatText.setEnabled(false); Cheatb = 0; QuestionActivity.CheatEn = Cheatb;}

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item);
        adapter.add(0);
        QuestionSpinner.setAdapter(adapter);

        dif = 1;
        Count = 0;
        L1 = 1;

        if (savedInstanceState != null) {
            Count = savedInstanceState.getInt("COUNTER_KEY", 0);
            dif = savedInstanceState.getInt("DIF_KEY",1);
            PregCount = savedInstanceState.getInt("PREG_KEY",0);
            L1 = savedInstanceState.getInt("LEVEL_KEY",1);
        }

        PregCount = dif*Count;

        if(Count > 0) {
            setResult(RESULT_OK);
        }




        AllCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (AllCheckBox.isChecked()){
                    Cat1CheckBox.setChecked(true);
                    Cat2CheckBox.setChecked(true);
                    Cat3CheckBox.setChecked(true);
                    Cat4CheckBox.setChecked(true);
                    Cat5CheckBox.setChecked(true);
                    Cat6CheckBox.setChecked(true);
                    ArrayAdapter<Integer> adapter2 = new ArrayAdapter<>(OptionActivity.this,R.layout.support_simple_spinner_dropdown_item);
                    adapter2.add(1);
                    adapter2.add(2);
                    adapter2.add(3);
                    adapter2.add(4);
                    adapter2.add(5);
                    adapter2.add(6);
                    adapter2.add(7);
                    adapter2.add(8);
                    adapter2.add(9);
                    adapter2.add(10);
                    adapter2.add(11);
                    adapter2.add(12);
                    adapter2.add(13);
                    adapter2.add(14);
                    adapter2.add(15);
                    QuestionSpinner.setAdapter(adapter2);
                    NoPreg = (int)QuestionSpinner.getSelectedItem();
                    QuestionActivity.Nopreg = NoPreg;
                    ResultActivity.NoPreg = NoPreg;
                }
            }
        });

        CheatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    CheatSpinner.setEnabled(true);
                    CheatText.setEnabled(true);
                    Cheatb = 1;
                    QuestionActivity.CheatEn = Cheatb;
                } else{  CheatSpinner.setEnabled(false); CheatText.setEnabled(false); Cheatb = 0; QuestionActivity.CheatEn = Cheatb;}

            }
        });

        QuestionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                NoPreg = (int)QuestionSpinner.getSelectedItem();
                QuestionActivity.Nopreg = NoPreg;
                ResultActivity.NoPreg = NoPreg;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        CheatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CheatC = (int)CheatSpinner.getSelectedItem();
                QuestionActivity.CheatC = CheatC;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (PregCount == 0){
            adapter.add(0);
        }

        if (PregCount == 1 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
        }
        if (PregCount == 2 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
            adapter.add(6);
            adapter.add(7);
            adapter.add(8);
            adapter.add(9);
            adapter.add(10);
        }
        if (PregCount >= 3 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
            adapter.add(6);
            adapter.add(7);
            adapter.add(8);
            adapter.add(9);
            adapter.add(10);
            adapter.add(11);
            adapter.add(12);
            adapter.add(13);
            adapter.add(14);
            adapter.add(15);
        }
        QuestionSpinner.setAdapter(adapter);

        NoPreg = (int)QuestionSpinner.getSelectedItem();
        QuestionActivity.Nopreg = NoPreg;
        QuestionActivity.CheatEn = Cheatb;
        QuestionActivity.CheatC = CheatC;
        ResultActivity.NoPreg = NoPreg;
        PregCount = dif*Count;
        QuestionClass.OptionLevel = L1;
        ResultActivity.Dif = L1;


    }

    public void onCategoryChecked(View v){
        List<CheckBox> Category= new ArrayList<>();
        Category.add(Cat1CheckBox);
        Category.add(Cat2CheckBox);
        Category.add(Cat3CheckBox);
        Category.add(Cat4CheckBox);
        Category.add(Cat5CheckBox);
        Category.add(Cat6CheckBox);
        Count = 0;
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item);

        for (CheckBox Categorys : Category){
            if(!Categorys.isChecked()){
                AllCheckBox.setChecked(false);
            }
        }

        for (CheckBox Categorys : Category) {
            if (Categorys.isChecked()) {
                Count = Count + 1;
            }
        }

        PregCount = dif*Count;

        if (PregCount == 0){
            adapter.add(0);
        }

        if (PregCount == 1 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
        }
        if (PregCount == 2 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
            adapter.add(6);
            adapter.add(7);
            adapter.add(8);
            adapter.add(9);
            adapter.add(10);
        }
        if (PregCount >= 3 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
            adapter.add(6);
            adapter.add(7);
            adapter.add(8);
            adapter.add(9);
            adapter.add(10);
            adapter.add(11);
            adapter.add(12);
            adapter.add(13);
            adapter.add(14);
            adapter.add(15);
        }

        QuestionSpinner.setAdapter(adapter);
        NoPreg = (int)QuestionSpinner.getSelectedItem();
        QuestionActivity.Nopreg = NoPreg;
        ResultActivity.NoPreg = NoPreg;

        if (Cat1CheckBox.isChecked()) {
            C1=1;
        }else{
            C1=0;
        }
        if (Cat2CheckBox.isChecked()){
            C2=1;
        }else{
            C2=0;
        }
        if (Cat3CheckBox.isChecked()){
            C3=1;
        }else{
            C3=0;
        }
        if (Cat4CheckBox.isChecked()){
            C4=1;
        }else{
            C4=0;
        }
        if (Cat5CheckBox.isChecked()){
            C5=1;
        }else{
            C5=0;
        }
        if (Cat6CheckBox.isChecked()){
            C6=1;
        }else{
            C6=0;
        }

        if (LowRadio.isChecked()){
            L1=1;
        }
        if (MediumRadio.isChecked()){
            L1=2;
        }
        if (HighRadio.isChecked()){
            L1=3;
        }
        QuestionClass.OptionLevel = L1;
        ResultActivity.Dif = L1;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("COUNTER_KEY",Count);
        outState.putInt("DIF_KEY",dif);
        outState.putInt("PREG_KEY",PregCount);
        outState.putInt("LEVEL_KEY",L1);
    }

    public void OnDifChanged(View v) {
        if(HighRadio.isChecked()){dif = 3;}
        if(MediumRadio.isChecked()){dif =2;}
        if(LowRadio.isChecked()){dif = 1;}

        List<CheckBox> Category= new ArrayList<>();
        Category.add(Cat1CheckBox);
        Category.add(Cat2CheckBox);
        Category.add(Cat3CheckBox);
        Category.add(Cat4CheckBox);
        Category.add(Cat5CheckBox);
        Category.add(Cat6CheckBox);
        Count = 0;
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item);

        for (CheckBox Categorys : Category){
            if(!Categorys.isChecked()){
                AllCheckBox.setChecked(false);
            }
        }

        for (CheckBox Categorys : Category) {
            if (Categorys.isChecked()) {
                Count = Count + 1;
            }
        }

        PregCount = dif*Count;

        if (PregCount == 0){
            adapter.add(0);
        }

        if (PregCount == 1 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
        }
        if (PregCount == 2 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
            adapter.add(6);
            adapter.add(7);
            adapter.add(8);
            adapter.add(9);
            adapter.add(10);
        }
        if (PregCount >= 3 ){
            adapter.add(1);
            adapter.add(2);
            adapter.add(3);
            adapter.add(4);
            adapter.add(5);
            adapter.add(6);
            adapter.add(7);
            adapter.add(8);
            adapter.add(9);
            adapter.add(10);
            adapter.add(11);
            adapter.add(12);
            adapter.add(13);
            adapter.add(14);
            adapter.add(15);
        }

        QuestionSpinner.setAdapter(adapter);
        NoPreg = (int)QuestionSpinner.getSelectedItem();
        QuestionActivity.Nopreg = NoPreg;
        ResultActivity.NoPreg = NoPreg;
        if (LowRadio.isChecked()){
            L1=1;
        }
        if (MediumRadio.isChecked()){
            L1=2;
        }
        if (HighRadio.isChecked()){
            L1=3;
        }
        QuestionClass.OptionLevel = L1;
        ResultActivity.Dif = L1;

    }

    @Override
    public void onBackPressed() {
        if(Count > 0){
        super.onBackPressed();}
        else { Toast.makeText(this, "Por favor, seleccione al menos una categoria", Toast.LENGTH_SHORT).show();}
    }
}
