package com.example.usuario.proyecto1;

import android.arch.lifecycle.ViewModel;

import java.util.List;

public class MainActivityModel extends ViewModel {
    private QuestionClass questionClass;
    private List<Question> gameQuestions;
    private int currentQuestionIndex;

    public int getCurrentQuestionIndex() { return currentQuestionIndex; }

    public void incrementCurrentQuestionIndex(){
        currentQuestionIndex = (currentQuestionIndex + 1) % gameQuestions.size();
    }

    public  void decrementCurrentQuestionIndex(){
        currentQuestionIndex = (currentQuestionIndex == 0) ? gameQuestions.size() - 1 : currentQuestionIndex - 1;
    }

    public void loadGameQuestion(){
        if (questionClass == null){
            currentQuestionIndex = 0;
            questionClass = new QuestionClass();
            gameQuestions = questionClass.getAllQuestionall();
        }
    }

    public Question getCurrentQuestion(){
        return gameQuestions.get(currentQuestionIndex);
    }

    public Question getNextQuestion(){
        incrementCurrentQuestionIndex();
        return gameQuestions.get(currentQuestionIndex);
    }

    public Question getPreviousQuiestion(){
        decrementCurrentQuestionIndex();
        return gameQuestions.get(currentQuestionIndex);
    }

}
