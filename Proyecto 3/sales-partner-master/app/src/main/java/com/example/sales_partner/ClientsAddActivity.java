package com.example.sales_partner;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sales_partner.dao.CategoryDao;
import com.example.sales_partner.dao.CustomerDao;
import com.example.sales_partner.dao.UserDao;
import com.example.sales_partner.databinding.ActivityClientsAddBinding;
import com.example.sales_partner.db.AppDatabase;
import com.example.sales_partner.model.Category;
import com.example.sales_partner.model.Customer;
import com.example.sales_partner.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class ClientsAddActivity extends AppCompatActivity {
    private boolean validationOn = false;

    //LOG
    private static final String TAG = "ClientsActivity";

    // DATA OBJECTS
    private CustomerDao customerDao;

    // Models
    private Customer customer;

    private boolean phone2Checked = false;
    private boolean phone3Checked = false;
    private boolean emailChecked = false;


    //////////////////////
    // GETTERS AND SETTERS
    /////////////////////

    public boolean isPhone2Checked() {
        return phone2Checked;
    }

    public void setPhone2Checked(boolean phone2Checked) {
        this.phone2Checked = phone2Checked;
    }

    public boolean isPhone3Checked() { return phone3Checked; }

    public void setPhone3Checked(boolean phone3Checked) { this.phone3Checked = phone3Checked; }


    public boolean isEmailChecked() { return emailChecked; }

    public void setEmailChecked(boolean emailChecked) { this.emailChecked = emailChecked; }


    // VIEW COMPONENTS
    EditText txtEditFirstName;
    EditText txtEditLastName;
    EditText txtEditPhone1;
    EditText txtEditPhone2;
    EditText txtEditPhone3;
    EditText tatEditAddress;
    EditText txtEditEmail;

//    Button btnCustomerSave;

    CheckBox checkBoxPhone2;
    CheckBox checkBoxPhone3;
    CheckBox checkBoxEmail;

    Spinner Status;

    private boolean saved;

    public static int ban = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_clients_add);



        ActivityClientsAddBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_clients_add);

        Status = findViewById(R.id.StatusClient);

        ArrayAdapter<String> StatusClients = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);
        StatusClients.add("Activo");
        StatusClients.add("Inactivo");
        Status.setAdapter(StatusClients);


        customer = (Customer)getIntent().getSerializableExtra("customer");
        if(customer==null){ customer = new Customer(); ban = 0;} else {ban = 1; Status.setSelection(customer.getStatus() - 1);}


        binding.setCustomer(customer);
        binding.setPhone2Checked(phone2Checked);

        customerDao = AppDatabase.getAppDatabase(getApplicationContext()).customerDao();

        Log.d(TAG, "onCreate: ");

        saved = false;


        // VIEW COMPONENTS INIT
        txtEditFirstName = findViewById(R.id.txtEditFirstName);
        txtEditLastName = findViewById(R.id.txtEditLastName);
        txtEditPhone1 = findViewById(R.id.txtEditPhone1);
        txtEditPhone2 = findViewById(R.id.txtEditPhone2);
        txtEditPhone3 = findViewById(R.id.txtEditPhone3);
        tatEditAddress = findViewById(R.id.txtEditAdress);
        txtEditEmail = findViewById(R.id.txtEditEmail);

//        btnCustomerSave = findViewById(R.id.btnCustomerSave);

        checkBoxPhone2 = findViewById(R.id.checkboxPhone2);
        checkBoxPhone3 = findViewById(R.id.checkboxPhone3);
        checkBoxEmail = findViewById(R.id.checkboxEmail);

    }
    //GENERATE TOOLBAR MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_customer, menu);
        return super.onCreateOptionsMenu(menu);
    }


    // SEARCH BUTTON ACTION
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Status = findViewById(R.id.StatusClient);
        switch (item.getItemId()) {
            case R.id.save_menu_item:
                /// SEARCH

                boolean enableSave;
                enableSave = true;

                //SAVING DATA
                String firstName = txtEditFirstName.getText().toString();
                String lastName = txtEditLastName.getText().toString();
                String phone1 = txtEditPhone1.getText().toString();
                String phone2 = txtEditPhone2.getText().toString();
                String phone3 = txtEditPhone3.getText().toString();
                String address = tatEditAddress.getText().toString();
                String email = txtEditEmail.getText().toString();

                String dialogString = "";

                if(validationOn){
                    if (firstName.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Nombre";
                    }
                    if (lastName.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Apellido";
                    }
                    if (phone1.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Telefono";
                    }
                    if (address.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Direccion";
                    }

                    if (checkBoxEmail.isChecked() && email.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Email";
                    }
                    if (checkBoxPhone2.isChecked() && phone2.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Telefono 2";
                    }
                    if (checkBoxPhone3.isChecked() && phone3.isEmpty()){
                        //MISSING DATA
                        enableSave = false;
                        dialogString += " Telefono 3";
                    }
                } else { enableSave = true; }



                if (enableSave){

                    //All data
                    List<Customer> customerList = customerDao.getAllById();
                    int id2 = customerList.get(customerList.size()-1).getId() + 1;
                    customer.setFirstName(firstName);
                    customer.setLastName(lastName);
                    customer.setPhone1(phone1);
                    customer.setPhone2(phone2);
                    customer.setPhone3(phone3);
                    customer.setAddress(address);
                    customer.setEmail(email);
                    customer.setStatus(Status.getSelectedItemPosition() + 1);
                    if(ban == 0){
                        customer.setId(id2);
                    customerDao.insertAll(customer);
                    jsonParseInsertClient(customer);}
                    else{
                        customerDao.update(customer);
                        jsonParseDeleteClient(customer);
                        jsonParseInsertClient(customer);
                    }

                    Toast.makeText(getApplicationContext(), "Datos guardados", Toast.LENGTH_SHORT).show();
                    Intent IntCustomers = new Intent(getApplicationContext(),ClientsActivity.class);
                    IntCustomers.putExtra("tag","start");
                    startActivity(IntCustomers);
                    finish();

                }else{
                    Toast.makeText(getApplicationContext(), "Falto llenar" + dialogString, Toast.LENGTH_SHORT).show();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //CONFIRM DELETE UNSAVED DATA
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.d(TAG, "onBackPressed: ");

        if (!saved) {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Cerrando ventana")
                    .setCancelable(false)
                    .setMessage("¿Está seguro que desea salir sin guardar")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent IntCustomers = new Intent(getApplicationContext(),ClientsActivity.class);
                            IntCustomers.putExtra("tag","start");
                            startActivity(IntCustomers);
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    private void jsonParseInsertClient(Customer customer) {

        String url = "http://148.209.151.91:3000/api/clients/";

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("id", customer.getId());
            jsonBody.put("first_name", customer.getFirstName());
            jsonBody.put("last_name", customer.getLastName());
            jsonBody.put("address", customer.getAddress());
            jsonBody.put("phone1", customer.getPhone1());
            jsonBody.put("phone2", customer.getPhone2());
            jsonBody.put("phone3", customer.getPhone3());
            jsonBody.put("e_mail", customer.getEmail());
            jsonBody.put("status", customer.getStatus());


            final String mRequestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("LOG_RESPONSE", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG_RESPONSE", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void jsonParseDeleteClient(Customer customer){

        String url = "http://148.209.151.91:3000/api/clients/" + customer.getId();

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        requestQueue.add(request);
    }

}