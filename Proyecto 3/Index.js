"use script";

const express = require('express');
var mysql = require('mysql');
const app = express(); //http mas complicada, puede nenviar cosas mas complicadas


var con = mysql.createConnection({
    host: "localhost",
    user: "Saenz1623",
    password: "Saenz1623",
    database: "inventorydb"
});

app.use(express.json());

//simular base de datos


app.get('/api/assemblies', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM assemblies';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/assembly_products', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM assembly_products';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/categories', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM product_categories';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/customers', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM customers';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/order_assemblies', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM order_assemblies';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/orders', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM orders';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/order_status', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM order_status';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/products', (req , res) => { // diagonal, ponen un numero, pide un id
    //envia texto en formato http
    var sql = 'SELECT * FROM products';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/users/:user', (req , res) => {
    //envia texto en formato http


    var user = req.params.user;
    var sql = 'SELECT * FROM usuarios where user LIKE ?';
    con.query(sql, [user], function (err, result) {
        if (err) throw err;
        res.send(result);
    });

});


app.post('/api/users/',(req,res) => {
    //validación
    const usuario = {user:req.body.user, password: req.body.password, name: req.body.name};
    res.send(usuario);
    var sql = "INSERT INTO usuarios (user, password, name) VALUES ?";
    var values = [[req.body.user,req.body.password, req.body.name]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;

    });
});



app.post('/api/clients/',(req,res) => {
    //validación

    var sql = "INSERT INTO customers (id, first_name, last_name, address, phone1, phone2, phone3, e_mail, status) VALUES ?";
    var values = [[req.body.id,req.body.first_name, req.body.last_name, req.body.address, req.body.phone1, req.body.phone2, req.body.phone3,req.body.e_mail, req.body.status]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.put('/api/clients/',(req,res) => {
    //validación

    var sql = "UPDATE customers SET first_name = '?' , last_name = '?', address = '?', phone1 = '?', phone2 = '?', phone3 = ?, e_mail = ? WHERE id = ?";
    var values = [[req.body.first_name, req.body.last_name, req.body.address, req.body.phone1, req.body.phone2, req.body.phone3,req.body.e_mail, req.body.id]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.delete('/api/clients/:id',(req,res) => {
    //validación

    var sql = "DELETE FROM CUSTOMERS WHERE id = ?";
    var values = [[req.params.id]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.delete('/api/orders/:id',(req,res) => {
    //validación

    var sql = "DELETE FROM orders WHERE id = ?";
    var values = [[req.params.id]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.delete('/api/orderAssem/:id',(req,res) => {
    //validación

    var sql = "DELETE FROM order_assemblies WHERE id = ?";
    var values = [[req.params.id]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.post('/api/orders/',(req,res) => {
    //validación

    var sql = "INSERT INTO orders (id, status_id, customer_id, date, change_log) VALUES ?";
    var values = [[req.body.id,req.body.status_id, req.body.customer_id, req.body.date, req.body.change_log]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.put('/api/orders/status',(req,res) => {
    //validación
    var status_id = req.body.status_id;
    var id = req.body.id;
    var sql = "UPDATE orders SET status_id = ? WHERE id = ?";
    con.query(sql,[status_id,id], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.put('/api/orders/change',(req,res) => {
    //validación
    var change_log = req.body.change_log;
    var id = req.body.id;
    var sql = "UPDATE orders SET change_log = ? WHERE id = ?";
    con.query(sql,[change_log,id], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});



app.post('/api/order_assemblies/',(req,res) => {
    //validación

    var sql = "INSERT INTO order_assemblies (id, assembly_id, qty) VALUES ?";
    var values = [[req.body.id,req.body.assembly_id, req.body.qty]];
    con.query(sql,[values], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.listen(3000,() => {
    console.log('Listening on port 3000...');
});


